//
//  MJGStack.m
//  MJGFoundation
//
//  Created by Matt Galloway on 06/01/2012.
//  Copyright (c) 2012 Matt Galloway. All rights reserved.
//

#if ! __has_feature(objc_arc)
#error This file requires ARC to be enabled. Either enable ARC for the entire project or use -fobjc-arc flag.
#endif

#import "SimpleStack.h"

@interface SimpleStack ()
@property (nonatomic, strong) NSMutableArray *objects;
@end

@implementation SimpleStack

int enumerationPos;

@synthesize objects = _objects;

#pragma mark -

- (id)init {
    if ((self = [self initWithArray:nil])) {
    }
    return self;
}

- (id)initWithArray:(NSArray*)array {
    if ((self = [super init])) {
        _objects = [[NSMutableArray alloc] initWithArray:array];
    }
    return self;
}


#pragma mark - Custom accessors

- (NSUInteger)count {
    return _objects.count;
}


#pragma mark -

- (void)pushObject:(id)object {
    if (object) {
        [_objects addObject:object];
    }
}

- (void)pushObjects:(NSArray*)objects {
    for (id object in objects) {
        [self pushObject:object];
    }
}

- (id)popObject {
    if (_objects.count > 0) {
        id object = [_objects objectAtIndex:(_objects.count - 1)];
        [_objects removeLastObject];
        return object;
    }
    return nil;
}

- (id)peekObject {
    if (_objects.count > 0) {
        id object = [_objects objectAtIndex:(_objects.count - 1)];
        return object;
    }
    return nil;
}


#pragma mark - NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id*)stackbuf count:(NSUInteger)len
{
    if(state->state == 0)
    {
        // state 0 means it's the first call, so get things set up
        
        // if count changes the array was modified
        state->mutationsPtr = &state->extra[0];//(unsigned long*)&count;
        
        //for our method we will use enumerationPos to track enumeration. Start at the back
        enumerationPos=[self count];
        
        // and update state to indicate that enumeration has started
        state->state = 1;
    }
    
    
    //count down then point to the next item in the list
    enumerationPos--;
    
    // if positon is <0 then we're done enumerating, return 0 to end
    if(enumerationPos < 0)
    {
        return 0;
    }
    
    else
    {
        __unsafe_unretained id currentItem=[_objects objectAtIndex:enumerationPos];
        
        // otherwise, point itemsPtr at the value
        state->itemsPtr = &currentItem;
        
        // we're returning exactly one item
        return 1;
    }
}

@end