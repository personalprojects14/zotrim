//
//  MainTabBarController.m
//  Zotrim
//
//  Created by Dexter Richard on 6/4/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "MainTabBarController.h"

@implementation MainTabBarController

- (IBAction)saveAndUnwindSegue:(UIStoryboardSegue *)segue
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    if ([self isMovingFromParentViewController])
    {
        MainNavigationController* parentVC=(MainNavigationController*)[self parentViewController];
        [parentVC userDidLogOut];
    }
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)context
{
    NSArray* viewControllers=[self viewControllers];
    
    for (ManagedObjectContextViewController* vc in viewControllers)
    {
        [vc setManagedObjectContext:context];
    }
    
    _managedObjectContext=context;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ManagedObjectContextViewController* nextVC=[segue destinationViewController];
    if ([nextVC respondsToSelector:@selector(setManagedObjectContext:)])
    {
        nextVC.managedObjectContext=_managedObjectContext;
    }
}

@end
