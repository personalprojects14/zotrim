//
//  FixedSizeQueue.h
//  Zotrim
//
//  Created by Dexter Richard on 4/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GenericSizedQueue <NSObject>

/**
 Push an object onto the queue. If the the queue is full, the item pushed off the front of the queue is removed and returned. Otherwise the return value is null.
 
 @param newObject The object to be pushed onto the queue.
 
 @return The item pushed off the queue if it's full, otherwise, NULL
 **/
- (id)enqueue:(id)newObject;
- (id)dequeue;
- (id)peek;


@end
