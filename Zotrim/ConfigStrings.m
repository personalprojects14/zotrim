//
//  ConfigStrings.m
//  Zotrim
//
//  Created by Dexter Richard on 2/3/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ConfigStrings.h"

@implementation ConfigStrings

NSString*  applicationDocumentsDirectory=@"MainUsersDocument";
NSString*  lowerCaseChars=@"abcdefghijklmnopqrstuvwxyz";
NSString*  upperCaseChars=@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ";
NSString*  numbers=@"0123456789";
NSString*  alphaNumeric=@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789";
NSString*  alpha=@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ";

-(ConfigStrings*)init
{
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"pt"] || [[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"pt-BR"])
    {
        registrationErrorMessage1=@"Você tem que entrar   ";
        registrationErrorMessage2=@" pra enregistrar uma conta nova.";
        usernameLengthErrorMessage=@"Seu nome de usario deve ter entre 6 e 16 letras.";
        usernameSpecialCharactersErrorMessage=@"Seu nome de usario nao pode ter nenhuma letra special.";
        usernameAlreadyInUseErrorMessage=@"Seu nome de usario escholido nao não está disponível";
        passwordLengthErrorMessage=@"Sua senha deve ser entre 8 e 24 letras.";
        passwordFormatErrorMessage=@"Sua senha deve ter ao menos uma letra maiscula, uma letra minuscula e um numero.";
        emailNeedsAmpersandErrorMessage=@"Seu enderesso de email deve ter uma '@'.";
        emailNeedsDomainSuffixErrorMessage=@"Seu enderesso de email nao tem o formato certo.";
        usernameNotFoundErrorMessage=@"Esse nome de usario nao foi achado. Verifique a ortographia.";
        incorrectPasswordErrorMessage=@"As senhas que você digitou não são iguais. Verifique a ortographia.";
        alphaError1=@"Seu ";
        alphaError2=@" nao pode ter as letras.";
        
        errorLoadingData=@"O applicativo encontrou um erro abrindo ";
        errorSavingData=@"O applicativo encontrou um erro gravando dados.";
        nextMealTimeNotEntered=@"    Proxima Refeição";
        lbsText=@" kg";
        passwordsDontMatchErrorMessage=@"As senhas que voce entrou nao sao eguais. Verifique a ortographia.";
        usernameToResetNotFoundErrorMessage=@"Nao tem uma conta com esse informações. Pour favor tentar de novo.";
        breakfastLabel=@"Café de Manhã";
        lunchLabel=@"Almoço";
        dinnerLabel=@"Janta";
        
        personalInfoSectionText=@"Modificar Informações Pessoais";
        accountInfoSectionText=@"Modificar Conta";
        appSettingsSectionText=@"Modificar Opções do App";
        remindersSectionText=@"Modificar Opções dos Lembretes";
        firstNameFieldText=@"Nome";
        lastNameFieldText=@"Sobrenome";
        ageFieldText=@"Idade";
        passwordFieldText=@"Senha";
        reEnterPasswordFieldText=@"Digite novamente sua Senha";
        emailFieldText=@"Email";
        autoLoginText=@"Fica logado quado o app fecha?";
        remindersForOtherUsersText=@"Mostra lembretes pra contas não logados?";
        yesText=@"SIM";
        noText=@"NAO";
        autoRemindersText=@"Automaticamente Criar Lembretes?";
        breakfastTimePickerTitle=@"Modiciar Horario de Café de Manhã:";
        lunchTimePickerTitle=@"Modificar Horario do Almoço:";
        dinnerTimePickerTitle=@"Modificar Horario da Janta:";
    }
    
    else
    {
        registrationErrorMessage1=@"You must enter your ";
        registrationErrorMessage2=@" to register for an account.";
        usernameLengthErrorMessage=@"Your Username must be between 6 and 16 characters long.";
        usernameSpecialCharactersErrorMessage=@"You Username must not contain any special characters";
        usernameAlreadyInUseErrorMessage=@"Your chosen username is already in use. Please Choose Another One.";
        passwordLengthErrorMessage=@"Your Password must be between 8 and 24 characters long.";
        passwordFormatErrorMessage=@"Your Password must contain an uppercase, lowercase and a number.";
        emailNeedsAmpersandErrorMessage=@"You have not entered a valid email address, it does not contain an '@'.";
        emailNeedsDomainSuffixErrorMessage=@"You have not entered a valid email address. It does not contain a valid domain name.";
        usernameNotFoundErrorMessage=@"The username you have enetered was not found. Please check your spelling and try again";
        incorrectPasswordErrorMessage=@"The password you enetered was not correct for this username. Please check your spelling and try again";
        alphaError1=@"Your ";
        alphaError2=@" must contain only numbers.";
        
        errorLoadingData=@"There was an error loading ";
        errorSavingData=@"There was an error saving data.";
        nextMealTimeNotEntered=@"    Next Meal";
        lbsText=@" lbs";
        passwordsDontMatchErrorMessage=@"The passwords you entered don't match, please check your spelling and try again.";
        usernameToResetNotFoundErrorMessage=@"No user was found matching this information. Please try again.";
        breakfastLabel=@"Breakfast";
        lunchLabel=@"Lunch";
        dinnerLabel=@"Dinner";
        
        personalInfoSectionText=@"Edit Personal Information";
        accountInfoSectionText=@"Edit Account Information";
        appSettingsSectionText=@"Edit General App Settings";
        remindersSectionText=@"Edit Reminder Settings";
        firstNameFieldText=@"First Name";
        lastNameFieldText=@"Last Name";
        ageFieldText=@"Age";
        passwordFieldText=@"Password";
        reEnterPasswordFieldText=@"Re-Enter Password";
        emailFieldText=@"Email Address";
        autoLoginText=@"Stay Logged In When App Closes?";
        remindersForOtherUsersText=@"Show Reminders For Logged Out Users?";
        yesText=@"YES";
        noText=@"NO";
        autoRemindersText=@"Automatically Generate Mealtime Reminders?";
        breakfastTimePickerTitle=@"Edit Breakfast Time:";
        lunchTimePickerTitle=@"Edit Lunch Time:";
        dinnerTimePickerTitle=@"Edit Dinner Time:";
    }
    
    return self;
}


@end