//
//  SecondViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManagedObjectContextViewController.h"
#import "CorePlot-CocoaTouch.h"
#import "PMEDatePicker.h"
#import "ConfigNumbers.h"
#import "ConfigStrings.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "WeightAtDate.h"

#import "User.h"
#import "ProgressGraph.h"
#import "GraphNode.h"
#import "StateInformation.h"
#import "PersonalInformation.h"

@interface ProgressTrackerViewController : ManagedObjectContextViewController <CPTPlotDataSource, UIPickerViewDataSource, UIPickerViewDelegate, PMEDatePickerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView* scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *graphHost;
@property (retain, nonatomic) CPTScatterPlot* plot;

@property (weak, nonatomic) IBOutlet PMEDatePicker *progressDatePicker;
@property (weak, nonatomic) IBOutlet PMEDatePicker *fromDatePicker;
@property (weak, nonatomic) IBOutlet PMEDatePicker *toDatePicker;

@property (weak, nonatomic) IBOutlet UIPickerView *weightPicker;

@property (retain, nonatomic) NSMutableArray* weightsList;
@property (atomic) NSInteger weightSelection;

@property (retain, nonatomic) NSArray* graphData;
@property (retain, nonatomic) NSArray* goalData;

- (IBAction)userDidAddProgress:(UIButton*)sender;

@end
