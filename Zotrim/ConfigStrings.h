//
//  ConfigStrings.h
//  Zotrim
//
//  Created by Dexter Richard on 2/3/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

@interface ConfigStrings : NSObject

@end

 NSString*  applicationDocumentsDirectory;

 NSString*  registrationErrorMessage1;
 NSString*  registrationErrorMessage2;
 NSString*  usernameLengthErrorMessage;
 NSString*  usernameSpecialCharactersErrorMessage;
 NSString*  usernameAlreadyInUseErrorMessage;
 NSString*  passwordFormatErrorMessage;
 NSString*  passwordLengthErrorMessage;
 NSString*  passwordsDontMatchErrorMessage;
 NSString*  emailNeedsAmpersandErrorMessage;
 NSString*  emailNeedsDomainSuffixErrorMessage;
 NSString*  alphaError1;
 NSString*  alphaError2;

 NSString*  nextMealTimeNotEntered;

 NSString*  usernameNotFoundErrorMessage;
 NSString*  incorrectPasswordErrorMessage;
 NSString*  usernameToResetNotFoundErrorMessage;

 NSString*  errorLoadingData;
 NSString*  errorSavingData;

 NSString*  lowerCaseChars;
 NSString*  upperCaseChars;
 NSString*  numbers;
 NSString*  alphaNumeric;
 NSString*  alpha;
 NSString*  lbsText;
 NSString*  breakfastLabel;
 NSString*  lunchLabel;
 NSString*  dinnerLabel;
 NSString*  yesText;
 NSString*  noText;

 NSString*  firstNameFieldText;
 NSString*  lastNameFieldText;
 NSString*  ageFieldText;

 NSString*  passwordFieldText;
 NSString*  reEnterPasswordFieldText;
 NSString*  emailFieldText;

 NSString*  autoLoginText;
 NSString*  remindersForOtherUsersText;

 NSString*  autoRemindersText;
 NSString*  breakfastTimePickerTitle;
 NSString*  lunchTimePickerTitle;
 NSString*  dinnerTimePickerTitle;

 NSString*  personalInfoSectionText;
 NSString*  accountInfoSectionText;
 NSString*  appSettingsSectionText;
 NSString*  remindersSectionText;