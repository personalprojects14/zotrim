//
//  FixedSizeQueue.m
//  Zotrim
//
//  Created by Dexter Richard on 2/21/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "FixedSizeQueue_MA.h"

@implementation FixedSizeQueue_MA

int enumerationPos;

- (instancetype)initWithMaxSize:(NSNumber*)maxSize
{
    _maxSize=maxSize;
    _contents=[[NSMutableArray alloc] initWithCapacity:[maxSize integerValue]];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    _maxSize=[aDecoder decodeObjectForKey:@"FixedSizeQueue_MAMaxSize"];
    _contents=[aDecoder decodeObjectForKey:@"FixedSizeQueue_MAContents"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_maxSize forKey:@"FixedSizeQueue_MAMaxSize"];
    [aCoder encodeObject:_contents forKey:@"FixedSizeQueue_MAContents"];
}

- (id)enqueue:(id)newObject
{
    //check to make sure it isnt already in the array. If so, remove existing one before we add it in the last position
    if ([self containsObject:newObject])
    {
        int indexOfDuplicate=[self indexOfObject:newObject];
        [self removeObjectAtIndex:indexOfDuplicate];
    }
    
    [self insertObject:newObject atIndex:0];

    if ([self isFull])
    {
        return [self dequeue];
    }
    
    else
    {
        return NULL;
    }
}

- (id)dequeue
{
    id dequeuedObject=[self lastObject];
    [self removeObjectAtIndex:[self count]-1];
    
    return dequeuedObject;
}

- (id)peek
{
    return [self lastObject];
}

- (BOOL)isFull
{
    if ([self count] >= (NSInteger)_maxSize)
    {
        return YES;
    }
    
    else
    {
        return NO;
    }
} 

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    if ([_contents respondsToSelector:aSelector])
    {
        return _contents;
    }
    
    else
    {
        return NULL;
    }
}

- (id)objectAtIndex:(NSUInteger)index
{
    if ([self count] > index)
    {
        return [_contents objectAtIndex:index];
    }
    
    else
    {
        return NULL;
    }
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id*)stackbuf count:(NSUInteger)len
{
    if(state->state == 0)
    {
        // state 0 means it's the first call, so get things set up
        
        // if count changes the array was modified
        state->mutationsPtr = &state->extra[0];//(unsigned long*)&count;
        
        //for our method we will use enumerationPos to track enumeration. Start at the back
        enumerationPos=[self count];
        
        // and update state to indicate that enumeration has started
        state->state = 1;
    }

    
    //count down then point to the next item in the list
    enumerationPos--;

    // if positon is <0 then we're done enumerating, return 0 to end
    if(enumerationPos < 0)
    {
        return 0;
    }
    
    else
    {
        __unsafe_unretained id currentItem=[_contents objectAtIndex:enumerationPos];
        
        // otherwise, point itemsPtr at the value
        state->itemsPtr = &currentItem;
        
        // we're returning exactly one item
        return 1;
    }
}

@end
