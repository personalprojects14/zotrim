//
//  FirstViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManagedObjectContextViewController.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "WeightAtDate.h"
#import "User.h"
#import "PersonalInformation.h"
#import "ConfigStrings.h"
#import "StateInformation.h"
#import "GraphNode.h"
#import "ProgressGraph.h"
#import "SimpleStack.h"
#import "AlarmObject.h"

@interface ProfileViewController : ManagedObjectContextViewController

@property (weak, nonatomic) IBOutlet UILabel *welcomeText;
@property (weak, nonatomic) IBOutlet UILabel *ageText;
@property (weak, nonatomic) IBOutlet UILabel *heightText;
@property (weak, nonatomic) IBOutlet UILabel *currentWeightText;
@property (weak, nonatomic) IBOutlet UILabel *goalWeightText;
@property (weak, nonatomic) IBOutlet UILabel *weight1Text;
@property (weak, nonatomic) IBOutlet UILabel *weight2Text;
@property (weak, nonatomic) IBOutlet UILabel *weight3Text;
@property (weak, nonatomic) IBOutlet UILabel *weight4Text;
@property (weak, nonatomic) IBOutlet UILabel *nextSupplementText;
@property (weak, nonatomic) IBOutlet UILabel *date1Text;
@property (weak, nonatomic) IBOutlet UILabel *date2Text;
@property (weak, nonatomic) IBOutlet UILabel *date3Text;
@property (weak, nonatomic) IBOutlet UILabel *date4Text;

@end
