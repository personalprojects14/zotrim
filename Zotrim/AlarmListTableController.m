//
//  AlarmListTableController.m
//  AlarmListTableController
//


#import "AlarmListTableController.h"
#import "AlarmObject.h"
#import "AddEditAlarmViewController.h"


@implementation AlarmListTableController

@synthesize tableView;
@synthesize imageView;
@synthesize listOfAlarms;

- (IBAction)cancelToAlarmListUnwindSegueAction:(UIStoryboardSegue *)segue
{
    [self.tableView reloadData];
    [self viewWillAppear:YES];
}

//
// viewDidLoad
//
// Configures the table after it is loaded.
//
- (void)viewWillAppear:(BOOL)animated
{
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	tableView.rowHeight = 70;
	tableView.backgroundColor = [UIColor clearColor];
    tableView.contentInset=UIEdgeInsetsZero;
    
    self.listOfAlarms = [AlarmListTableController alarmsForCurrentUserWithManagedObjectContext:[self managedObjectContext]];
    
    [tableView reloadData];
}

+ (NSMutableArray*)alarmsForCurrentUserWithManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *alarmListData = [defaults objectForKey:@"AlarmListData"];
    NSMutableArray* allAlarms=[NSKeyedUnarchiver unarchiveObjectWithData:alarmListData];
    NSMutableArray* alarmsForCurrentUser=[[NSMutableArray alloc] init];
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext: managedObjectContext];
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:managedObjectContext];
    NSString* currentUsername=[info username];
    
    for (AlarmObject* alarm in allAlarms)
    {
       if ([[alarm alarmOwner] isEqualToString:currentUsername])
       {
           [alarmsForCurrentUser addObject:alarm];
       }
    }
    
    if ([[settings autoReminders] boolValue] == YES)
    {
        AlarmObject* breakfastAlarm;
        AlarmObject* lunchAlarm;
        AlarmObject* dinnerAlarm;
    
        for (AlarmObject* alarm in alarmsForCurrentUser)
        {
            if ([[alarm label] isEqualToString:breakfastLabel])
            {
                breakfastAlarm=alarm;
                [breakfastAlarm setTimeToSetOff:[info breakfastTime]];
                
                [AddEditAlarmViewController cancelExistingNotificationWithID:[breakfastAlarm notificationID]];
                [AddEditAlarmViewController scheduleLocalNotificationWithDate:[breakfastAlarm timeToSetOff] atIndex:[breakfastAlarm notificationID] withManagedObjectContext:managedObjectContext];
            }
            
            else if ([[alarm label] isEqualToString:lunchLabel])
            {
                lunchAlarm=alarm;
                [lunchAlarm setTimeToSetOff:[info lunchTime]];
                
                [AddEditAlarmViewController cancelExistingNotificationWithID:[lunchAlarm notificationID]];
                [AddEditAlarmViewController scheduleLocalNotificationWithDate:[lunchAlarm timeToSetOff] atIndex:[lunchAlarm notificationID] withManagedObjectContext:managedObjectContext];
            }
            
            else if ([[alarm label] isEqualToString:dinnerLabel])
            {
                dinnerAlarm=alarm;
                [dinnerAlarm setTimeToSetOff:[info dinnerTime]];
                
                [AddEditAlarmViewController cancelExistingNotificationWithID:[dinnerAlarm notificationID]];
                [AddEditAlarmViewController scheduleLocalNotificationWithDate:[dinnerAlarm timeToSetOff] atIndex:[dinnerAlarm notificationID] withManagedObjectContext:managedObjectContext];
            }
        }
        
        if (breakfastAlarm == NULL)
        {
            AlarmObject* newAlarm=[[AlarmObject alloc] init];
            [newAlarm setLabel:breakfastLabel];
            [newAlarm setTimeToSetOff:[info breakfastTime]];
            [newAlarm setEnabled:YES];
            [newAlarm setNotificationID:[self getUniqueNotificationID]];
            [newAlarm setAlarmOwner:[info username]];
            
            [alarmsForCurrentUser addObject:newAlarm];
            [allAlarms addObject:newAlarm];
            
            [AddEditAlarmViewController scheduleLocalNotificationWithDate:[newAlarm timeToSetOff] atIndex:[self getUniqueNotificationID] withManagedObjectContext:managedObjectContext];
        }
        
        if (lunchAlarm == NULL)
        {
            AlarmObject* newAlarm=[[AlarmObject alloc] init];
            [newAlarm setLabel:lunchLabel];
            [newAlarm setTimeToSetOff:[info lunchTime]];
            [newAlarm setEnabled:YES];
            [newAlarm setNotificationID:[self getUniqueNotificationID]];
            [newAlarm setAlarmOwner:[info username]];
            
            [alarmsForCurrentUser addObject:newAlarm];
            [allAlarms addObject:newAlarm];
            
            [AddEditAlarmViewController scheduleLocalNotificationWithDate:[newAlarm timeToSetOff] atIndex:[self getUniqueNotificationID] withManagedObjectContext:managedObjectContext];
        }
        
        if (dinnerAlarm == NULL)
        {
            AlarmObject* newAlarm=[[AlarmObject alloc] init];
            [newAlarm setLabel:dinnerLabel];
            [newAlarm setTimeToSetOff:[info dinnerTime]];
            [newAlarm setEnabled:YES];
            [newAlarm setNotificationID:[self getUniqueNotificationID]];
            [newAlarm setAlarmOwner:[info username]];
            
            [alarmsForCurrentUser addObject:newAlarm];
            [allAlarms addObject:newAlarm];
            
            [AddEditAlarmViewController scheduleLocalNotificationWithDate:[newAlarm timeToSetOff] atIndex:[self getUniqueNotificationID] withManagedObjectContext:managedObjectContext];
        }
        
        NSData *alarmListData2 = [NSKeyedArchiver archivedDataWithRootObject:allAlarms];
        [[NSUserDefaults standardUserDefaults] setObject:alarmListData2 forKey:@"AlarmListData"];
    }
    
    [info setAlarmsForUser:alarmsForCurrentUser];

    NSError* error;
    if (![managedObjectContext save:&error])
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:errorSavingData];
    }
    
    return alarmsForCurrentUser;
}

//
// numberOfSectionsInTableView:
//
// Return the number of sections for the table.
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

//
// tableView:numberOfRowsInSection:
//
// Returns the number of rows in a given section.
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.listOfAlarms){
        
        return [self.listOfAlarms count];
    }
    else return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"AlarmListToEditAlarm" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if([segue.identifier isEqualToString:@"AlarmListToEditAlarm"])
    {
        AddEditAlarmViewController *controller = (AddEditAlarmViewController *)segue.destinationViewController;
        controller.indexOfAlarmToEdit = tableView.indexPathForSelectedRow.row;
        controller.editMode = YES;
    }
}
//
// tableView:cellForRowAtIndexPath:
//
// Returns the cell for a given indexPath.
//
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDateFormatter * dateReader = [[NSDateFormatter alloc] init];
    [dateReader setDateFormat:@"hh:mm a"];
    AlarmObject *currentAlarm = [self.listOfAlarms objectAtIndex:indexPath.row];
    
    NSString *label = currentAlarm.label;
    BOOL enabled = currentAlarm.enabled;
    NSString *date = [dateReader stringFromDate:currentAlarm.timeToSetOff];
    
    
	UILabel *topLabel;
	UILabel *bottomLabel;
    UISwitch *enabledSwitch;

	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];

		//
		// Create the cell.
		//
		cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];

		//
		// Create the label for the top row of text
		//
		topLabel =
			[[UILabel alloc]
				initWithFrame:
              CGRectMake(14,5,170,40)];
		[cell.contentView addSubview:topLabel];

		//
		// Configure the properties for the text that are the same on every row
		//
		topLabel.backgroundColor = [UIColor clearColor];
		topLabel.textColor =[UIColor colorWithRed:.30 green:.04 blue:.69 alpha:1.0];
		topLabel.highlightedTextColor = [UIColor whiteColor];
		topLabel.font = [UIFont systemFontOfSize:[UIFont labelFontSize]+2];

		//
		// Create the label for the top row of text
		//
		bottomLabel =
			[[UILabel alloc]
				initWithFrame:
					CGRectMake(14,30,170,40)];
		[cell.contentView addSubview:bottomLabel];

		//
		// Configure the properties for the text that are the same on every row
		//
		bottomLabel.backgroundColor = [UIColor clearColor];
		bottomLabel.textColor = [UIColor blackColor];
		bottomLabel.highlightedTextColor = [UIColor whiteColor];
		bottomLabel.font = [UIFont systemFontOfSize:[UIFont labelFontSize]];

        
        enabledSwitch = [[UISwitch alloc]
                         initWithFrame:
                         CGRectMake(200,20,170,40)];
        enabledSwitch.tag = indexPath.row;
        [enabledSwitch addTarget:self
                            action:@selector(toggleAlarmEnabledSwitch:)
                  forControlEvents:UIControlEventTouchUpInside];
        
		[cell.contentView addSubview:enabledSwitch];
        
        [enabledSwitch setOn:enabled];
        topLabel.text = date;
        bottomLabel.text = label;

	
	return cell;
}

-(void)toggleAlarmEnabledSwitch:(id)sender
{
    UISwitch *mySwitch = (UISwitch *)sender;
    
    if(mySwitch.isOn == NO)
    {
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *eventArray = [app scheduledLocalNotifications];
        AlarmObject *currentAlarm = [self.listOfAlarms objectAtIndex:mySwitch.tag];
        currentAlarm.enabled = NO;
        for (int i=0; i<[eventArray count]; i++)
        {
            UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
            NSDictionary *userInfoCurrent = oneEvent.userInfo;
            NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"notificationID"]];
            if ([uid isEqualToString:[NSString stringWithFormat:@"%i",mySwitch.tag]])
            {
                //Cancelling local notification            
                [app cancelLocalNotification:oneEvent];
                break;
            }
        }
        
    }
    
    else if(mySwitch.isOn == YES)
    {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        AlarmObject *currentAlarm = [self.listOfAlarms objectAtIndex:mySwitch.tag];
        currentAlarm.enabled = YES;
        if (!localNotification)
            return;

        localNotification.repeatInterval = NSDayCalendarUnit;
        [localNotification setFireDate:currentAlarm.timeToSetOff];
        [localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
        // Setup alert notification
        [localNotification setAlertBody:@"Alarm" ];
        [localNotification setAlertAction:@"Open App"];
        [localNotification setHasAction:YES];
        
        
        NSNumber* uidToStore = [NSNumber numberWithInt:currentAlarm.notificationID];
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:uidToStore forKey:@"notificationID"];
        localNotification.userInfo = userInfo;
        
        // Schedule the notification
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    NSData *alarmListData2 = [NSKeyedArchiver archivedDataWithRootObject:self.listOfAlarms];
    [[NSUserDefaults standardUserDefaults] setObject:alarmListData2 forKey:@"AlarmListData"];
}

@end


