//
//  MainTabBarController.h
//  Zotrim
//
//  Created by Dexter Richard on 6/4/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainNavigationController.h"

@interface MainTabBarController : UITabBarController

@property (retain,nonatomic) NSManagedObjectContext* managedObjectContext;

- (IBAction)saveAndUnwindSegue:(UIStoryboardSegue*)segue;

- (void)setManagedObjectContext:(NSManagedObjectContext*)context;

@end
