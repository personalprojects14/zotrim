//
//  ResetPasswordViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 2/2/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()

@end

@implementation ResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([self shouldUpdatePassword])
    {
        //update password
        [self updatePassword];
        return YES;
    }
    
    else
    {
        return NO;
    }
}

- (BOOL)shouldUpdatePassword
{
    NSString* newPass1=[_nPasswordField text];
    NSString* newPass2=[_confirmPasswordField text];
    
    if ([newPass1 isEqualToString:newPass2])
    {
        return YES;
    }
    
    else
    {
        [UINavigationController displayErrorPopupWithMessage:passwordsDontMatchErrorMessage];
        return NO;
    }
}

- (void)updatePassword
{
    [SimpleKeychain save:_username data:[_nPasswordField text]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupTextFields];
}

-(void)setupTextFields
{
    _usernameText.text=[[_usernameText text] stringByAppendingString:_username];
    
    [_nPasswordField setShouldAdvance:YES];
    [_confirmPasswordField setShouldAdvance:NO];
    
    self.textFields=[[NSArray alloc] initWithObjects:_nPasswordField,_confirmPasswordField, nil];
}

- (BOOL)textFieldShouldReturn:(AdvancingUITextField *)textField
{
    //check if we are at the end, and if so (RETURNS YES) perform submit action as if it came from _resetButton
    BOOL shouldReturn=[super textFieldShouldReturn:textField];
    if (shouldReturn)
    {
        if ([self shouldPerformSegueWithIdentifier:@"SuccessfulPasswordReset" sender:self])
        {
            [self performSegueWithIdentifier:@"SuccessfulPasswordReset" sender:self];
        }
    }
    
    return shouldReturn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
