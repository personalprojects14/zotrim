//
//  User.m
//  Zotrim
//
//  Created by Dexter Richard on 6/24/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "User.h"
#import "PersonalInformation.h"
#import "ProgressGraph.h"
#import "UserSettings.h"


@implementation User

@dynamic info;
@dynamic progressGraph;
@dynamic userSettings;

@end
