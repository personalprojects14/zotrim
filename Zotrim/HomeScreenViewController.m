//
//  HomeScreenViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HomeScreenViewController.h"

@interface HomeScreenViewController ()

@end

@implementation HomeScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loginUnwindSegueAction:(UIStoryboardSegue*)segue
{
    //use segue.sourceViewController and do stuff with VC before it goes away
    //LoginViewController* login=segue.sourceViewController;
}

- (void)cancelUnwindSegueAction:(UIStoryboardSegue*)segue
{
    
}

- (void)recentsButtonWasPressed:(UIButton*)recentsButton
{
    [self performSegueWithIdentifier:@"LoginAttemptWithUsername" sender:recentsButton];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"LoginAttemptWithUsername"])
    {
        UIButton* buttonPressed=(UIButton*)sender;
        NSString* username=[[buttonPressed titleLabel] text];
        LoginViewController* loginVC=(LoginViewController*)[segue destinationViewController];
        [loginVC autofill:username];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self createAttributedRecentsLabel];
    [self hideAllRecents];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self managedObjectContext] != NULL)
    {
        if ([self shouldSegueToProfile])
        {
            [self segueToProfile];
        }
        
        else
        {
            [self displayRecents];
        }
    }
}

- (void)userDidLogOut
{
    NSManagedObjectContext* context=[self managedObjectContext];
    NSEntityDescription* entityDescription=[NSEntityDescription entityForName:@"StateInformation" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];
    
    if (array != NULL)
    {
        StateInformation* stateInfo=[array firstObject];
        
        if ([stateInfo.loggedIn compare:[NSNumber numberWithBool:YES]] == NSOrderedSame)
        {
            stateInfo.loggedIn=[NSNumber numberWithBool:NO];
            stateInfo.currentUsername=NULL;
            
            NSError* error;
            if (![[self managedObjectContext] save:&error])
            {
                //handle the error
                [UINavigationController displayErrorPopupWithMessage:errorSavingData];
            }
        }
    }
}

- (BOOL)shouldSegueToProfile
{
    NSManagedObjectContext* context=[self managedObjectContext];
    NSEntityDescription* entityDescription=[NSEntityDescription entityForName:@"StateInformation" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];

    if (array != NULL)
    {
        StateInformation* stateInfo=[array firstObject];
        
        if (stateInfo != NULL)
        {
            if ([stateInfo.loggedIn compare:[NSNumber numberWithBool:YES]] == NSOrderedSame)
            {
                return YES;
            }
            
            else
            {
                return NO;
            }

        }
        
        else
        {
            return NO;
        }
    }
    
    else
    {
        return NO;
    }
}

- (void)segueToProfile
{
    [self performSegueWithIdentifier:@"LoggedIn" sender:self];
}

- (void)createAttributedRecentsLabel
{
    NSDictionary *attributes = @{NSUnderlineStyleAttributeName: @1};
    
    NSAttributedString *myString = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"RECENTS_LABEL", @"Recents") attributes:attributes];
    self.recents.attributedText = myString;
}

- (void)hideAllRecents
{
    [_recentsLabel1 setHidden:YES];
    [_recentsLabel2 setHidden:YES];
    [_recentsLabel3 setHidden:YES];
    [_recentsLabel4 setHidden:YES];
}

- (void)displayRecents
{
    NSManagedObjectContext* context=[self managedObjectContext];
    NSEntityDescription* entityDescription=[NSEntityDescription entityForName:@"StateInformation" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];
    
    if (array != NULL)
    {
        StateInformation* stateInfo=[array firstObject];
        FixedSizeQueue* recentUsers=stateInfo.recentUsers;
        NSString* user1=[recentUsers objectAtIndex:0];
        NSString* user2=[recentUsers objectAtIndex:1];
        NSString* user3=[recentUsers objectAtIndex:2];
        NSString* user4=[recentUsers objectAtIndex:3];
        
        if (user1 != NULL)
        {
            [_recentsLabel1 setTitle:user1 forState:UIControlStateNormal];
            [_recentsLabel1 setHidden: NO];
        }
        
        if (user2 != NULL)
        {
            [_recentsLabel2 setTitle:user2 forState:UIControlStateNormal];
            [_recentsLabel2 setHidden:NO];
        }
        
        if (user3 != NULL)
        {
            [_recentsLabel3 setTitle:user3 forState:UIControlStateNormal];
            [_recentsLabel3 setHidden:NO];
        }
        
        if (user4 != NULL)
        {
            [_recentsLabel4 setTitle:user4 forState:UIControlStateNormal];
            [_recentsLabel4 setHidden:NO];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
