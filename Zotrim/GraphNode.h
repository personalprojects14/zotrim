//
//  GraphNode.h
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GraphNode, ProgressGraph;

@interface GraphNode : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) ProgressGraph *parentGraphOld;
@property (nonatomic, retain) GraphNode *nextNode;
@property (nonatomic, retain) GraphNode *previousNode;
@property (nonatomic, retain) ProgressGraph *parentGraphNew;

@end
