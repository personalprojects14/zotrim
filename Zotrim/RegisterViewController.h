//
//  RegisterViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HidesKeyboardOnTapUIViewController.h"
#import "FormattedPlaceholderTextField.h"
#import "LoginViewController.h"
#import "User.h"
#import "PersonalInformation.h"
#import "LoginInformation.h"
#import "ConfigStrings.h"
#import "SimpleKeychain.h"
#import "ProgressGraph.h"
#import "GraphNode.h"
#import "AlarmObject.h"
#import "UserSettings.h"
#import "AlarmListTableController.h"

#import "UINavigationController+SimpleErrorMessage.h"

@interface RegisterViewController : HidesKeyboardOnTapUIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *heightPicker;

@property (weak, nonatomic) IBOutlet UILabel *breakfastTimeLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *breakfastTimePicker;
@property (weak, nonatomic) IBOutlet UILabel *lunchTimeLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *lunchTimePicker;
@property (weak, nonatomic) IBOutlet UILabel *dinnerTimeLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *dinnerTimePicker;

@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* firstNameField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* lastNameField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* usernameField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* passwordField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField *repeatPasswordField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* emailField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* ageField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* currentWeightField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField* goalWeightField;

@property (weak, nonatomic) IBOutlet UISwitch *autoRemindersSwitch;
@property (weak, nonatomic) IBOutlet UIButton* submitButton;


- (IBAction)switchValueChanged:(id)sender;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
