//
//  MainNavigationController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreenViewController.h"

@interface MainNavigationController : UINavigationController

- (void)userDidLogOut;

@end
