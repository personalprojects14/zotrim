//
//  ConfigNumbers.h
//  Zotrim
//
//  Created by Dexter Richard on 6/9/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

extern NSInteger const minimumWeightLbs;
extern NSInteger const maximumWeightLbs;

extern NSInteger const firstMonth;
extern NSInteger const firstDay;
extern NSInteger const firstYear;

extern double const graphBufferX;
extern double const graphBufferY;

extern NSInteger const initialGraphWhitespaceX;
extern NSInteger const initialGraphWhitespaceY;

extern NSInteger const xAxisTickIntervalDivisor;

extern double const animationFPS;

extern double const purpleColorRed;
extern double const purpleColorGreen;
extern double const purpleColorBlue;

extern int const DEFAULT_ROW_HEIGHT;
extern int const PICKER_ROW_HEIGHT;