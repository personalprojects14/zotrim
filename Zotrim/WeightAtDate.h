//
//  WeightAtDate.h
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeightAtDate : NSObject

@property (retain, nonatomic) NSNumber* weight;
@property (retain, nonatomic) NSDate* date;

@end
