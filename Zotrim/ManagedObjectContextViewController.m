//
//  ManagedObjectContextViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 2/3/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ManagedObjectContextViewController.h"

@interface ManagedObjectContextViewController ()

@end

@implementation ManagedObjectContextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ManagedObjectContextViewController* nextVC=[segue destinationViewController];
    if ([nextVC respondsToSelector:@selector(setManagedObjectContext:)])
    {
        nextVC.managedObjectContext=_managedObjectContext;
    }
}

+ (PersonalInformation*)loadCurrentUserInfoFromManagedObjectContext:(NSManagedObjectContext*)context
{
    User* user=[self loadCurrentUserFromManagedObjectContext:context];

    return [user info];
}

+ (User*)loadCurrentUserFromManagedObjectContext:(NSManagedObjectContext*)context
{
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"StateInformation" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];
    
    StateInformation* stateInfo;
    if (array == NULL)
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"StateInformation."]];
        return NULL;
    }
    
    else if ([array count] == 0)
    {
        //something funny is going on
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"StateInformation."]];
        return NULL;
    }
    
    else
    {
        stateInfo=[array firstObject];
    }
    
    NSString* username=[stateInfo currentUsername];
    
    entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(info.username == %@)", username];
    [request setPredicate:predicate];
    
    array=[context executeFetchRequest:request error:&error];
    
    User* user;
    if (array == NULL)
    {
        //something funny is going on
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"Username1."]];
        return NULL;
    }
    
    else if ([array count] == 0)
    {
        //something else funny is going on
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"Username2."]];
        return NULL;
    }
    
    else
    {
        user=[array firstObject];
    }
    
    return user;
}

+ (StateInformation*)loadCurrentStateInfoFromManagedObjectContext:(NSManagedObjectContext *)context
{
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"StateInformation" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];
    
    StateInformation* stateInfo;
    if (array == NULL)
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"StateInformation."]];
    }
    
    else if ([array count] == 0)
    {
        //this is the first run of the app, create state information object
        stateInfo=[NSEntityDescription insertNewObjectForEntityForName:@"StateInformation" inManagedObjectContext:context];
        stateInfo.recentUsers=[[FixedSizeQueue alloc] initWithMaxSize:[NSNumber numberWithInt:4]];
    }
    
    else
    {
        stateInfo=[array firstObject];
    }
    
    return  stateInfo;
}

+ (UserSettings*)loadCurrentUserSettingsFromManagedObjectContext:(NSManagedObjectContext *)context
{
    User* user=[self loadCurrentUserFromManagedObjectContext:context];
    
    return [user userSettings];
}

@end
