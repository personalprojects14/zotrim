//
//  ForgotAccountViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 2/1/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ForgotAccountViewController.h"

@interface ForgotAccountViewController ()

@property (strong, nonatomic) NSArray* heightPickerListFt;
@property (strong, nonatomic) NSArray* heightPickerListIn;

@property (weak, nonatomic) NSNumber* feetRow;
@property (weak, nonatomic) NSNumber* inchesRow;

@end

@implementation ForgotAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"ResetPasswordAttempt"])
    {
        ResetPasswordViewController* nextVC=(ResetPasswordViewController*)[segue destinationViewController];
        [nextVC setUsername:_username];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"ResetPasswordAttempt"])
    {
        if ([self shouldResetPassword])
        {
            [self resetPasswordForUser:_username];
            return YES;
        }
        
        else
        {
            return NO;
        }
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)shouldResetPassword
{
    NSString* email=_emailField.text;
    NSString* firstName=_firstnameField.text;
    NSString* feet=[[_heightPickerListFt objectAtIndex:[_feetRow integerValue]] string];
    NSString* inches=[[_heightPickerListIn objectAtIndex:[_inchesRow integerValue]] string];
    NSString* height=[feet stringByAppendingString:inches];
    
    NSManagedObjectContext* context=[self managedObjectContext];
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSFetchRequest* request=[[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(info.email == %@) && (info.firstName == %@) && (info.height == %@)", email, firstName, height];
    [request setPredicate:predicate];
    
    NSError* error;
    NSArray* array=[context executeFetchRequest:request error:&error];
    
    if (array == NULL)
    {
        //something funny is going on
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"Username1."]];
        return NO;
    }
    
    else if ([array count] == 0)
    {
        //no user matches
        [UINavigationController displayErrorPopupWithMessage:usernameToResetNotFoundErrorMessage];
        return NO;
    }
    
    else if ([array count] > 1)
    {
        //something else funny is going one
        //something funny is going on
        [UINavigationController displayErrorPopupWithMessage:[errorLoadingData stringByAppendingString:@"Username2."]];
        return NO;
    }
    
    else
    {
        //reset the password to a temporary random value
        User* user=[array firstObject];
        [self setUsername:[[user info] username]];
        return YES;
    }
}

- (void)resetPasswordForUser:(NSString*)username
{
    [SimpleKeychain save:username data:[NSNumber numberWithInt:arc4random()]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initPickerLists];
    [self setupTextFields];
}

- (void)initPickerLists
{
    NSDictionary* attributes=@{NSFontAttributeName:[UIFont boldSystemFontOfSize:10],NSForegroundColorAttributeName:[UIColor colorWithRed:.19 green:.17 blue:.4 alpha:1.0]};
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0]  isEqual: @"pt"] || [[[NSLocale preferredLanguages] objectAtIndex:0]  isEqual: @"pt-BR"])
    {
        _heightPickerListFt=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"1 m" attributes:attributes],[[NSAttributedString alloc] initWithString:@"2 m" attributes:attributes],nil];
        
        NSMutableArray *cmList=[[NSMutableArray alloc] init];
        
        for (int i=0; i <= 100; i++)
        {
            [cmList addObject:[[NSAttributedString alloc] initWithString:[[[NSString alloc] initWithString:[[NSNumber numberWithInt:i] stringValue]] stringByAppendingString:@" m"] attributes:attributes]];
        }
    }
    
    else
    {
        _heightPickerListFt=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"4\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"5\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"6\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"7\'" attributes:attributes], nil];
        _heightPickerListIn=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"0\"" attributes:attributes], [[NSAttributedString alloc] initWithString:@"1\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"2\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"3\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"4\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"5\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"6\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"7\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"8\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"9\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"10\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"11\"" attributes:attributes], nil];
    }
}

- (void)setupTextFields
{
    [_emailField setShouldAdvance:YES];
    [_firstnameField setShouldAdvance:NO];
    
    self.textFields=[[NSArray alloc] initWithObjects:_emailField, _firstnameField, nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==0)//first coulmn
    {
        return [_heightPickerListFt count];
    }
    
    else //second column
    {
        return [_heightPickerListIn count];
    }
}

- (NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==0)//feet column
    {
        return [_heightPickerListFt objectAtIndex:row];
    }
    
    else //inches column
    {
        return [_heightPickerListIn objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component==0)//feet column
    {
        _feetRow=[NSNumber numberWithInt:row];
    }
    
    else //inches column
    {
        _inchesRow=[NSNumber numberWithInt:row];
    }
}

- (BOOL)textFieldShouldReturn:(AdvancingUITextField *)textField
{
    //check if we are at the end, and if so (RETURNS YES) perform submit action as if it came from _resetButton
    BOOL shouldReturn=[super textFieldShouldReturn:textField];
    if (shouldReturn)
    {
        if ([self shouldPerformSegueWithIdentifier:@"ResetPasswordAttempt" sender:_resetButton])
        {
            [self performSegueWithIdentifier:@"ResetPasswordAttempt" sender:_resetButton];
        }
    }
    
    return shouldReturn;
}


@end
