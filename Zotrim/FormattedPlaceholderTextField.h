//
//  FormattedPlaceholderTextField.h
//  Zotrim
//
//  Created by Dexter Richard on 1/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvancingUITextField.h"

@interface FormattedPlaceholderTextField : AdvancingUITextField

@end
