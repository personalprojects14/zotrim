//
//  StateInformation.m
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "StateInformation.h"


@implementation StateInformation

@dynamic currentUsername;
@dynamic loggedIn;
@dynamic recentUsers;

@end
