//
//  ForgotAccountViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 2/1/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HidesKeyboardOnTapUIViewController.h"
#import "FormattedPlaceholderTextField.h"
#import "ConfigStrings.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "User.h"
#import "PersonalInformation.h"
#import "SimpleKeychain.h"
#import "ResetPasswordViewController.h"

#import <stdlib.h>

@interface ForgotAccountViewController : HidesKeyboardOnTapUIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *heightPicker;

@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField *emailField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField *firstnameField;

@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (weak, nonatomic) NSString* username;

@end
