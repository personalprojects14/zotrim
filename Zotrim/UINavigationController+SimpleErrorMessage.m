//
//  UINavigationController+SimpleErrorMessage.m
//  Zotrim
//
//  Created by Dexter Richard on 2/21/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "UINavigationController+SimpleErrorMessage.h"

@implementation UINavigationController (SimpleErrorMessage)

+ (void)displayErrorPopupWithMessage:(NSString*)message
{
    [[[UIAlertView alloc] initWithTitle:@"Error" message: message delegate:NULL cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
