//
//  SettingsViewControler.m
//  Zotrim
//
//  Created by Dexter Richard on 6/27/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "SettingsViewControler.h"

NSInteger const AUTOLOGIN_YESNOSWITCH_TAG=0;
NSInteger const LOGGEDOUTREMINDERS_YESNOSWITCH_TAG=1;
NSInteger const AUTOREMINDERS_YESNOSWITCH_TAG=2;

@implementation SettingsViewControler

- (void)viewWillAppear:(BOOL)animated
{
    [self setupCategoriesAndRows];
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"saveAndUnwindSegue"])
    {
        return [self isUserInputValid];
    }
    
    else
    {
        return YES;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"saveAndUnwindSegue"])
    {
        [self saveUserData];
        
    }
    
    [_mocVC prepareForSegue:segue sender:sender];
}

- (void)saveUserData
{
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:[self managedObjectContext]];
    if (![[_firstNameField text] isEqualToString:@""])
    {
        info.firstName=[_firstNameField text];
    }
    
    if (![[_lastNameField text] isEqualToString:@""])
    {
        info.lastName=[_lastNameField text];
    }
    
    if (![[_ageField text] isEqualToString:@""])
    {
        info.age=[NSNumber numberWithInt:[[_ageField text] integerValue]];
    }
    
    if (![[_emailField text] isEqualToString:@""])
    {
        info.email=[_emailField text];
    }
    
    if (![[_passwordField text] isEqualToString:@""])
    {
        [SimpleKeychain save:[info username] data:[_passwordField text]];
    }
    
    if (_autoLogin != nil)
    {
        settings.stayLoggedIn=_autoLogin;
    }
    
    if (_loggedOutReminders != nil)
    {
        settings.loggedOutReminders=_loggedOutReminders;
    }
    
    if (_autoReminders != nil)
    {
        settings.autoReminders=_autoReminders;
    }
    
    info.breakfastTime=[_breakfastTimePicker date];
    info.lunchTime=[_lunchTimePicker date];
    info.dinnerTime=[_dinnerTimePicker date];
    
    [AlarmListTableController alarmsForCurrentUserWithManagedObjectContext:[self managedObjectContext]];
    [AppDelegate refreshViews];
}

- (BOOL)isUserInputValid
{
    return ([self isNameValid] && [self isPasswordValid] && [self isEmailValid] && [self isAgeValid]);
}

- (BOOL)isNameValid
{
//    if ([_firstNameField.text isEqualToString:@""])
//    {
//        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:@"First Name"] stringByAppendingString: registrationErrorMessage2]];
//        return NO;
//    }
//    
//    else if ([_lastNameField.text isEqualToString:@""])
//    {
//        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:@"Last Name"] stringByAppendingString: registrationErrorMessage2]];
//        return NO;
//    }
//    
//    else
//    {
    
    // any values are acceptable here so just return yes
    return YES;
//    }
}

- (BOOL)isPasswordValid
{
    if ([_passwordField.text isEqualToString:@""])
    {
        return YES;
    }
    
    if ([_passwordField.text length]>24 || [_passwordField.text length]<8)
    {
        [UINavigationController displayErrorPopupWithMessage:passwordLengthErrorMessage];
        return NO;
    }
    
    else if ([_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:lowerCaseChars]].location == NSNotFound || [_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:upperCaseChars]].location == NSNotFound || [_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:numbers]].location == NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:passwordFormatErrorMessage];
        return NO;
    }
    
    else if (![_passwordField.text isEqualToString:_reEnterPasswordField.text])
    {
        [UINavigationController displayErrorPopupWithMessage:passwordsDontMatchErrorMessage];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isEmailValid
{
    if ([_emailField.text isEqualToString:@""])
    {
        return YES;
    }
    
    if ([_emailField.text rangeOfString:@"@"].location == NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:emailNeedsAmpersandErrorMessage];
        return NO;
    }
    
    else if ([_emailField.text rangeOfString:@".com"].location == NSNotFound && [_emailField.text rangeOfString:@".net"].location == NSNotFound && [_emailField.text rangeOfString:@".org"].location == NSNotFound && [_emailField.text rangeOfString:@".gov"].location == NSNotFound && [_emailField.text rangeOfString:@".edu"].location == NSNotFound )
    {
        [UINavigationController displayErrorPopupWithMessage:emailNeedsDomainSuffixErrorMessage];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isAgeValid
{
    if ([_ageField.text isEqualToString:@""])
    {
        return YES;
    }
    
    if ([_ageField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:alpha]].location != NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:[[alphaError1 stringByAppendingString:@"age"] stringByAppendingString: alphaError2]];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (void)setupCategoriesAndRows
{
    NSArray* sections=[[NSArray alloc] initWithObjects:personalInfoSectionText, accountInfoSectionText, appSettingsSectionText, remindersSectionText, nil];
    super.sections=sections;
    
    NSMutableArray* sectionInfoArray=[[NSMutableArray alloc] initWithObjects:[self setupPersonalInformationSection], [self setupAccountInfoSection], [self setupAppSettingsSection], [self setupReminderSettingsSection], nil];
    super.sectionInfoArray=sectionInfoArray;
}

- (APLSectionInfo*)setupPersonalInformationSection
{
    APLSectionInfo* personalInformationSection=[[APLSectionInfo alloc] init];
    [personalInformationSection setSectionLabel:personalInfoSectionText];
    [personalInformationSection setOpen:NO];
    NSNumber *defaultRowHeight = @(DEFAULT_ROW_HEIGHT);
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    
    APLTableCell* firstNameCell=[[APLTableCell alloc] init];
    [personalInformationSection insertObject:defaultRowHeight inRowHeightsAtIndex:0];
    _firstNameLabel=[[UILabel alloc] init];
    [_firstNameLabel setText:firstNameFieldText];
    [firstNameCell setLabel:_firstNameLabel];
    _firstNameField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_firstNameField setBorderStyle:UITextBorderStyleRoundedRect];
    [_firstNameField setShouldAdvance:YES];
    [_firstNameField setDelegate:_mocVC];
    [_firstNameField setPlaceholder:[info firstName]];
    [firstNameCell setContent:_firstNameField];
    
    APLTableCell* lastNameCell=[[APLTableCell alloc] init];
    [personalInformationSection insertObject:defaultRowHeight inRowHeightsAtIndex:1];
    _lastNameLabel=[[UILabel alloc] init];
    [_lastNameLabel setText:lastNameFieldText];
    [lastNameCell setLabel:_lastNameLabel];
    _lastNameField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_lastNameField setBorderStyle:UITextBorderStyleRoundedRect];
    [_lastNameField setShouldAdvance:YES];
    [_lastNameField setDelegate:_mocVC];
    [_lastNameField setPlaceholder:[info lastName]];
    [lastNameCell setContent:_lastNameField];
    
    APLTableCell* ageCell=[[APLTableCell alloc] init];
    [personalInformationSection insertObject:defaultRowHeight inRowHeightsAtIndex:2];
    _ageLabel=[[UILabel alloc] init];
    [_ageLabel setText:ageFieldText];
    [ageCell setLabel:_ageLabel];
    _ageField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_ageField setBorderStyle:UITextBorderStyleRoundedRect];
    [_ageField setShouldAdvance:NO];
    [_ageField setDelegate:_mocVC];
    [_ageField setPlaceholder:[[info age] stringValue]];
    [ageCell setContent:_ageField];
    
    //add the text fields to the list in the mocVC
    [_mocVC setTextFields:[[NSArray alloc] initWithObjects:_firstNameField, _lastNameField, _ageField, nil]];
    
    [personalInformationSection setCellsForSection:[[NSArray alloc] initWithObjects:firstNameCell, lastNameCell, ageCell, nil]];
    [personalInformationSection setNumberOfRowsInSection:[[personalInformationSection cellsForSection] count]];
    
    return personalInformationSection;
}

- (APLSectionInfo*)setupAccountInfoSection
{
    APLSectionInfo* accountInfoSection=[[APLSectionInfo alloc] init];
    [accountInfoSection setSectionLabel:accountInfoSectionText];
    [accountInfoSection setOpen:NO];
    NSNumber *defaultRowHeight = @(DEFAULT_ROW_HEIGHT);
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    
    APLTableCell* passwordCell=[[APLTableCell alloc] init];
    [accountInfoSection insertObject:defaultRowHeight inRowHeightsAtIndex:0];
    _passwordLabel=[[UILabel alloc] init];
    [_passwordLabel setText:passwordFieldText];
    [passwordCell setLabel:_passwordLabel];
    _passwordField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_passwordField setBorderStyle:UITextBorderStyleRoundedRect];
    [_passwordField setShouldAdvance:YES];
    [_passwordField setDelegate:_mocVC];
    [_passwordField setSecureTextEntry:YES];
    [passwordCell setContent:_passwordField];
    
    APLTableCell* reEnterPasswordCell=[[APLTableCell alloc] init];
    [accountInfoSection insertObject:defaultRowHeight inRowHeightsAtIndex:1];
    _reEnterPasswordLabel=[[UILabel alloc] init];
    [_reEnterPasswordLabel setText:reEnterPasswordFieldText];
    [reEnterPasswordCell setLabel:_reEnterPasswordLabel];
    _reEnterPasswordField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_reEnterPasswordField setBorderStyle:UITextBorderStyleRoundedRect];
    [_reEnterPasswordField setShouldAdvance:YES];
    [_reEnterPasswordField setDelegate:_mocVC];
    [_reEnterPasswordField setSecureTextEntry:YES];
    [reEnterPasswordCell setContent:_reEnterPasswordField];
    
    APLTableCell* emailCell=[[APLTableCell alloc] init];
    [accountInfoSection insertObject:defaultRowHeight inRowHeightsAtIndex:2];
    _emailLabel=[[UILabel alloc] init];
    [_emailLabel setText:emailFieldText];
    [emailCell setLabel:_emailLabel];
    _emailField=[[FormattedPlaceholderTextField alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 30)];
    [_emailField setBorderStyle:UITextBorderStyleRoundedRect];
    [_emailField setShouldAdvance:NO];
    [_emailField setDelegate:_mocVC];
    [_emailField setPlaceholder:[info email]];
    [emailCell setContent:_emailField];
    
    //add the text fields to the list in the mocVC
    NSMutableArray* textFields=[[NSMutableArray alloc] initWithArray:[_mocVC textFields]];
    [textFields addObject:_passwordField];
    [textFields addObject:_reEnterPasswordField];
    [textFields addObject:_emailField];
    [_mocVC setTextFields:textFields];
    
    [accountInfoSection setCellsForSection:[[NSArray alloc] initWithObjects:passwordCell, reEnterPasswordCell, emailCell, nil]];
    [accountInfoSection setNumberOfRowsInSection:[[accountInfoSection cellsForSection] count]];
    
    return accountInfoSection;
}

- (APLSectionInfo*)setupAppSettingsSection
{
    APLSectionInfo* appSettingsSection=[[APLSectionInfo alloc] init];
    [appSettingsSection setSectionLabel:appSettingsSectionText];
    [appSettingsSection setOpen:NO];
    NSNumber *defaultRowHeight = @(DEFAULT_ROW_HEIGHT);
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:[self managedObjectContext]];
    
    APLTableCell* autoLoginCell=[[APLTableCell alloc] init];
    [appSettingsSection insertObject:defaultRowHeight inRowHeightsAtIndex:0];
    _autoLoginLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_autoLoginLabel setText:autoLoginText];
    [autoLoginCell setLabel:_autoLoginLabel];
    _autoLoginSwitchView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.9f, 40)];
    _yesLabel=[[UILabel alloc] initWithFrame:CGRectMake(105, 0, 50, 30)];
    [_yesLabel setText:yesText];
    _noLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [_noLabel setText:noText];
    _autoLoginYesNoSwitch=[[UISwitch alloc] initWithFrame:CGRectMake(40, 0, 60, 30)];
    [_autoLoginYesNoSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_autoLoginYesNoSwitch setTag:AUTOLOGIN_YESNOSWITCH_TAG];
    [_autoLoginYesNoSwitch setOn:[[settings stayLoggedIn] boolValue]];
    [_autoLoginSwitchView addSubview:_noLabel];
    [_autoLoginSwitchView addSubview:_autoLoginYesNoSwitch];
    [_autoLoginSwitchView addSubview:_yesLabel];
    [autoLoginCell setContent:_autoLoginSwitchView];
    
    APLTableCell* loggedOutRemindersCell=[[APLTableCell alloc] init];
    [appSettingsSection insertObject:defaultRowHeight inRowHeightsAtIndex:1];
    _loggedOutRemindersLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_loggedOutRemindersLabel setText:remindersForOtherUsersText];
    [loggedOutRemindersCell setLabel:_loggedOutRemindersLabel];
    _loggedOutRemindersSwitchView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.9f, 40)];
    _yesLabel=[[UILabel alloc] initWithFrame:CGRectMake(105, 0, 50, 30)];
    [_yesLabel setText:yesText];
    _noLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [_noLabel setText:noText];
    _loggedOutRemindersYesNoSwitch=[[UISwitch alloc] initWithFrame:CGRectMake(40, 0, 60, 30)];
    [_loggedOutRemindersYesNoSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_loggedOutRemindersYesNoSwitch setTag:LOGGEDOUTREMINDERS_YESNOSWITCH_TAG];
    [_loggedOutRemindersYesNoSwitch setOn:[[settings loggedOutReminders] boolValue]];
    [_loggedOutRemindersSwitchView addSubview:_noLabel];
    [_loggedOutRemindersSwitchView addSubview:_loggedOutRemindersYesNoSwitch];
    [_loggedOutRemindersSwitchView addSubview:_yesLabel];
    [loggedOutRemindersCell setContent:_loggedOutRemindersSwitchView];
    
    [appSettingsSection setCellsForSection:[[NSArray alloc] initWithObjects:autoLoginCell, loggedOutRemindersCell, nil]];
    [appSettingsSection setNumberOfRowsInSection:[[appSettingsSection cellsForSection] count]];
    
    return appSettingsSection;
}

- (APLSectionInfo*)setupReminderSettingsSection
{
    APLSectionInfo* reminderSettingsSection=[[APLSectionInfo alloc] init];
    [reminderSettingsSection setSectionLabel:remindersSectionText];
    [reminderSettingsSection setOpen:NO];
    NSNumber *defaultRowHeight = @(DEFAULT_ROW_HEIGHT);
    NSNumber *pickerRowHeight = @(PICKER_ROW_HEIGHT);
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:[self managedObjectContext]];
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    
    APLTableCell* autoRemindersCell=[[APLTableCell alloc] init];
    [reminderSettingsSection insertObject:defaultRowHeight inRowHeightsAtIndex:0];
    _autoRemindersLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_autoRemindersLabel setText:autoRemindersText];
    [autoRemindersCell setLabel:_autoRemindersLabel];
    _autoRemindersSwitchView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.9f, 40)];
    _yesLabel=[[UILabel alloc] initWithFrame:CGRectMake(105, 0, 50, 30)];
    [_yesLabel setText:yesText];
    _noLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [_noLabel setText:noText];
    _autoRemindersYesNoSwitch=[[UISwitch alloc] initWithFrame:CGRectMake(40, 0, 60, 30)];
    [_autoRemindersYesNoSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_autoRemindersYesNoSwitch setTag:AUTOREMINDERS_YESNOSWITCH_TAG];
    [_autoRemindersYesNoSwitch setOn:[[settings autoReminders] boolValue] animated:NO];
    [_autoRemindersSwitchView addSubview:_noLabel];
    [_autoRemindersSwitchView addSubview:_autoRemindersYesNoSwitch];
    [_autoRemindersSwitchView addSubview:_yesLabel];
    [autoRemindersCell setContent:_autoRemindersSwitchView];
    
    APLTableCell* breakfastTimeCell=[[APLTableCell alloc] init];
    [reminderSettingsSection insertObject:pickerRowHeight inRowHeightsAtIndex:1];
    _breakfastTimePickerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_breakfastTimePickerLabel setText:breakfastTimePickerTitle];
    [breakfastTimeCell setLabel:_breakfastTimePickerLabel];
    _breakfastTimePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 300, 162)];
    [_breakfastTimePicker setDatePickerMode:UIDatePickerModeTime];
    [_breakfastTimePicker setDate:[info breakfastTime]];
    [breakfastTimeCell setContent:_breakfastTimePicker];
    
    APLTableCell* lunchTimeCell=[[APLTableCell alloc] init];
    [reminderSettingsSection insertObject:pickerRowHeight inRowHeightsAtIndex:2];
    _lunchTimePickerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_lunchTimePickerLabel setText:lunchTimePickerTitle];
    [lunchTimeCell setLabel:_lunchTimePickerLabel];
    _lunchTimePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 300, 162)];
    [_lunchTimePicker setDatePickerMode:UIDatePickerModeTime];
    [_lunchTimePicker setDate:[info lunchTime]];
    [lunchTimeCell setContent:_lunchTimePicker];
    
    APLTableCell* dinnerTimeCell=[[APLTableCell alloc] init];
    [reminderSettingsSection insertObject:pickerRowHeight inRowHeightsAtIndex:3];
    _dinnerTimePickerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, ([[UIScreen mainScreen] bounds].size.width)*0.86f, 35)];
    [_dinnerTimePickerLabel setText:dinnerTimePickerTitle];
    [dinnerTimeCell setLabel:_dinnerTimePickerLabel];
    _dinnerTimePicker=[[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 300, 162)];
    [_dinnerTimePicker setDatePickerMode:UIDatePickerModeTime];
    [_dinnerTimePicker setDate:[info dinnerTime]];
    [dinnerTimeCell setContent:_dinnerTimePicker];
    
    [reminderSettingsSection setCellsForSection:[[NSArray alloc] initWithObjects:autoRemindersCell, breakfastTimeCell, lunchTimeCell, dinnerTimeCell, nil]];
    [reminderSettingsSection setNumberOfRowsInSection:[[reminderSettingsSection cellsForSection] count]];
    
    return reminderSettingsSection;
}

- (IBAction)switchValueChanged:(id)sender
{
    UISwitch* selectedSwitch=(UISwitch*)sender;
    
    switch (selectedSwitch.tag)
    {
        case AUTOLOGIN_YESNOSWITCH_TAG:
            if ([selectedSwitch isOn])
            {
                _autoLogin=[NSNumber numberWithBool:YES];
            }
            
            else
            {
                _autoLogin=[NSNumber numberWithBool:NO];
            }
            break;
            
        case LOGGEDOUTREMINDERS_YESNOSWITCH_TAG:
            if ([selectedSwitch isOn])
            {
                _loggedOutReminders=[NSNumber numberWithBool:YES];
            }
            
            else
            {
                _loggedOutReminders=[NSNumber numberWithBool:NO];
            }
            break;
            
        case AUTOREMINDERS_YESNOSWITCH_TAG:
            if ([selectedSwitch isOn])
            {
                _autoReminders=[NSNumber numberWithBool:YES];
            }
            
            else
            {
                _autoReminders=[NSNumber numberWithBool:NO];
            }
            break;
            
        default:
            break;
    }
}

- (NSManagedObjectContext*)managedObjectContext
{
    return [_mocVC managedObjectContext];
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (_mocVC == NULL)
    {
        _mocVC=[[HidesKeyboardOnTapUIViewController alloc] init];
    }
    
    [_mocVC setManagedObjectContext:managedObjectContext];
}

- (void)resignOnTap:(id)sender
{
    [HidesKeyboardOnTapUIViewController viewController:self resignOnTap:sender];
}

@end
