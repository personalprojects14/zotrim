//
//  UINavigationController+SimpleErrorMessage.h
//  Zotrim
//
//  Created by Dexter Richard on 2/21/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (SimpleErrorMessage)

+ (void)displayErrorPopupWithMessage:(NSString*)message;

@end
