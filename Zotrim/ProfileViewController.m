//
//  FirstViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setupTextFields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self displayNextAlarmTime];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTextFields
{
    PersonalInformation* currentInfo=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    
    NSString* firstName=[currentInfo firstName];
    _welcomeText.text=[_welcomeText.text stringByAppendingString:firstName];
    _ageText.text=[[currentInfo age] stringValue];
    _heightText.text=[currentInfo height];
    _currentWeightText.text=[[[self retrieveCurrentWeightForUser:[currentInfo user]] stringValue] stringByAppendingString:lbsText];
    _goalWeightText.text=[[[currentInfo goalWeight] stringValue] stringByAppendingString:lbsText];

    NSArray* recentWeightText=[[NSArray alloc] initWithObjects:_weight1Text, _weight2Text, _weight3Text, _weight4Text ,nil];
    NSArray* recentDateText=[[NSArray alloc] initWithObjects:_date1Text, _date2Text, _date3Text, _date4Text, nil];
    [_weight1Text setHidden:YES];
    [_weight2Text setHidden:YES];
    [_weight3Text setHidden:YES];
    [_weight4Text setHidden:YES];
    [_date1Text setHidden:YES];
    [_date2Text setHidden:YES];
    [_date3Text setHidden:YES];
    [_date4Text setHidden:YES];
    
    SimpleStack* recentWeights=[self retrieveRecentWeightsForUser:[currentInfo user]];
    int i=0;
    for (WeightAtDate* pair in recentWeights)
    {
        UILabel* weightText=[recentWeightText objectAtIndex:i];
        [weightText setHidden:NO];
        weightText.text=[[[pair weight] stringValue] stringByAppendingString:lbsText];
        UILabel* dateText=[recentDateText objectAtIndex:i];
        [dateText setHidden:NO];
        NSDate* date=[pair date];
        NSDateComponents* components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth fromDate:date];
        NSString* dateString=[[[NSString stringWithFormat:@"%d", [components month]] stringByAppendingString:@"/"] stringByAppendingString:[NSString stringWithFormat:@"%d", [components day]]];
        dateText.text=dateString;
        
        i++;
    }
    
    [self displayNextAlarmTime];
}

- (SimpleStack*)retrieveRecentWeightsForUser:(User*)user
{
    ProgressGraph* graph=[user progressGraph];
    GraphNode* node=[graph newestNode];
    SimpleStack* recentWeights=[[SimpleStack alloc] init];
    
    while (node != NULL && [recentWeights count] < 4)
    {
        WeightAtDate* pair=[[WeightAtDate alloc] init];
        [pair setWeight:[node weight]];
        [pair setDate:[node date]];
        
        [recentWeights pushObject:pair];
        
        node=[node previousNode];
    }
    
    return recentWeights;
}

- (NSNumber*)retrieveCurrentWeightForUser:(User*)user
{
    ProgressGraph* graph=[user progressGraph];
    GraphNode* firstNode=[graph newestNode];
    
    return [firstNode weight];
}

- (void)displayNextAlarmTime
{
    NSDate* nextSupplement=[self nextSupplementTime];
    if (nextSupplement != NULL)
    {
        NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE, h:mm a"];
        
        NSCalendar* currentCalendar=[NSCalendar currentCalendar];
        NSDateComponents* componenetsOfNow=[currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[NSDate date]];
        NSDate* now=[currentCalendar dateFromComponents:componenetsOfNow];
        NSDateComponents* componentsOfAlarm=[currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:nextSupplement];
        componenetsOfNow=[currentCalendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        if ([nextSupplement compare:now] == NSOrderedAscending)//if the time is before now, its tomorrow's alarm
        {
            NSDate* tomorrow=[NSDate dateWithTimeIntervalSinceNow:60*60*24];
            NSDateComponents* componentsOfTomorrow=[currentCalendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tomorrow];
            
            [componentsOfAlarm setDay:[componentsOfTomorrow day]];
            [componentsOfAlarm setMonth:[componentsOfTomorrow month]];
            [componentsOfAlarm setYear:[componentsOfTomorrow year]];
        }
        
        else //it's an alarm for today
        {
            [componentsOfAlarm setDay:[componenetsOfNow day]];
            [componentsOfAlarm setMonth:[componenetsOfNow month]];
            [componentsOfAlarm setYear:[componenetsOfNow year]];
        }
        
        nextSupplement=[currentCalendar dateFromComponents:componentsOfAlarm];
        
        _nextSupplementText.text=[formatter stringFromDate:nextSupplement];
    }
    
    else
    {
        _nextSupplementText.text=nextMealTimeNotEntered;
    }
}

- (NSDate*)nextSupplementTime
{
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    NSArray* alarmsForUser=[info alarmsForUser];
    
    NSDate* nextSupplementTime=[[alarmsForUser firstObject] timeToSetOff];
    NSDate* earliestAlarm=[[alarmsForUser firstObject] timeToSetOff];
   
    NSCalendar* currentCalendar=[NSCalendar currentCalendar];
    NSDateComponents* componenetsOfNow=[currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[NSDate date]];
    NSDate* now=[currentCalendar dateFromComponents:componenetsOfNow];
    
    for (AlarmObject* alarm in alarmsForUser)
    {
        NSDateComponents* componentsOfNextSupplementTime=[currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:nextSupplementTime];
        nextSupplementTime=[currentCalendar dateFromComponents:componentsOfNextSupplementTime];
        
        NSDateComponents* componentsOfAlarmTime=[currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[alarm timeToSetOff]];
        NSDate* alarmTime=[currentCalendar dateFromComponents:componentsOfAlarmTime];
        
       if ([nextSupplementTime compare:now] == NSOrderedAscending && [alarmTime compare:now] == NSOrderedDescending)
       {
           nextSupplementTime=alarmTime;
       }
        
       else if ([alarmTime compare:now] == NSOrderedDescending && [alarmTime compare:nextSupplementTime] == NSOrderedAscending)
       {
           nextSupplementTime=alarmTime;
       }
        
       if ([alarmTime compare:earliestAlarm] == NSOrderedAscending)
       {
           earliestAlarm=alarmTime;
       }
    }
    
    if ([nextSupplementTime compare:now] == NSOrderedAscending)
    {
        nextSupplementTime=earliestAlarm;//if nothing is coming up today, next is the first one tomorrow
    }
    
    return nextSupplementTime;
}

@end
