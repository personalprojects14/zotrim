//
//  User.h
//  Zotrim
//
//  Created by Dexter Richard on 6/24/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PersonalInformation, ProgressGraph, UserSettings;

@interface User : NSManagedObject

@property (nonatomic, retain) PersonalInformation *info;
@property (nonatomic, retain) ProgressGraph *progressGraph;
@property (nonatomic, retain) UserSettings *userSettings;

@end
