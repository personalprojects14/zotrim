//
//  AppDelegate.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManagedObjectContextViewController.h"
#import "ConfigStrings.h"
#import "HomeScreenViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (retain, nonatomic) NSManagedObjectContext* managedObjectContext;

+ (void)refreshViews;

@end

