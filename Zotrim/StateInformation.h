//
//  StateInformation.h
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StateInformation : NSManagedObject

@property (nonatomic, retain) NSString * currentUsername;
@property (nonatomic, retain) NSNumber * loggedIn;
@property (nonatomic, retain) id recentUsers;

@end
