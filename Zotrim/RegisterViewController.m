//
//  RegisterViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@property (strong, nonatomic) NSArray* genderPickerList;
@property (strong, nonatomic) NSArray* heightPickerListFt;
@property (strong, nonatomic) NSArray* heightPickerListIn;

@property (weak, nonatomic) UITextField* activeField;

@property (weak, nonatomic) NSString* genderValue;
@property (weak, nonatomic) NSNumber* feetRow;
@property (weak, nonatomic) NSNumber* inchesRow;

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self registerForKeyBoardNotifications];
    [self initPickerLists];
    [self hideMealtimePickers];
    [self setupTextFields];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqual:@"RegistrationAttempt"])
    {
        return [self isUserInputValid];
    }
    
    else
    {
        return YES;
    }
}

- (IBAction)switchValueChanged:(id)sender
{
    if ([sender isEqual:_autoRemindersSwitch])
    {
        if ([_autoRemindersSwitch isOn])
        {
            [self showMealtimePickers];
        }
        
        else //deslected
        {
            [self hideMealtimePickers];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isEqual:_submitButton])
    {
        [self createNewAccountFromEneteredData];
        [super prepareForSegue:segue sender:sender];
        LoginViewController* loginVC=(LoginViewController*)[segue destinationViewController];
        [loginVC autofill:[_usernameField text]];
    }
}

- (void)setupTextFields
{
    [_firstNameField setShouldAdvance:YES];
    [_lastNameField setShouldAdvance:YES];
    [_usernameField setShouldAdvance:YES];
    [_passwordField setShouldAdvance:YES];
    [_repeatPasswordField setShouldAdvance:YES];
    [_emailField setShouldAdvance:YES];
    [_ageField setShouldAdvance:YES];
    [_currentWeightField setShouldAdvance:YES];
    [_goalWeightField setShouldAdvance:NO];
    
    self.textFields=[[NSArray alloc] initWithObjects:_firstNameField,_lastNameField,_usernameField,_passwordField,_repeatPasswordField,_emailField,_ageField,_currentWeightField,_goalWeightField, nil];
}

- (void)initPickerLists
{
    NSDictionary* attributes=@{NSFontAttributeName:[UIFont boldSystemFontOfSize:10],NSForegroundColorAttributeName:[UIColor colorWithRed:.19 green:.17 blue:.4 alpha:1.0]};
    
    _genderPickerList=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"MALE_TEXT", @"MALE_TEXT") attributes:attributes],[[NSAttributedString alloc] initWithString:NSLocalizedString(@"FEMALE_TEXT", @"FEMALE_TEXT") attributes:attributes],[[NSAttributedString alloc] initWithString:NSLocalizedString(@"NOT_SPECIFIED", @"NOT_SPECIFIED") attributes:attributes], nil];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0]  isEqual: @"pt"] || [[[NSLocale preferredLanguages] objectAtIndex:0]  isEqual: @"pt-BR"])
    {
        _heightPickerListFt=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"1 m" attributes:attributes],[[NSAttributedString alloc] initWithString:@"2 m" attributes:attributes],nil];
        
        NSMutableArray *cmList=[[NSMutableArray alloc] init];
        
        for (int i=0; i <= 100; i++)
        {
            [cmList addObject:[[NSAttributedString alloc] initWithString:[[[NSString alloc] initWithString:[[NSNumber numberWithInt:i] stringValue]] stringByAppendingString:@" cm"] attributes:attributes]];
        }
        
        _heightPickerListIn=cmList;
    }
    
    else
    {
        _heightPickerListFt=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"4\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"5\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"6\'" attributes:attributes],[[NSAttributedString alloc] initWithString:@"7\'" attributes:attributes], nil];
        _heightPickerListIn=[[NSArray alloc] initWithObjects:[[NSAttributedString alloc] initWithString:@"0\"" attributes:attributes], [[NSAttributedString alloc] initWithString:@"1\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"2\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"3\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"4\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"5\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"6\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"7\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"8\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"9\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"10\"" attributes:attributes],[[NSAttributedString alloc] initWithString:@"11\"" attributes:attributes], nil];
    }
}

- (void)hideMealtimePickers
{
    [_breakfastTimeLabel setHidden:YES];
    [_breakfastTimePicker setHidden:YES];
    [_lunchTimeLabel setHidden:YES];
    [_lunchTimePicker setHidden:YES];
    [_dinnerTimeLabel setHidden:YES];
    [_dinnerTimePicker setHidden:YES];
    
    CGSize adjustedSize=self.contentView.bounds.size;
    adjustedSize.height-=[self heightOfMealtimePickers];
    
    //self.scrollView.contentSize=adjustedSize;
    
    //add animation
    [UIView animateWithDuration:0.5 animations:^{self.scrollView.contentSize=adjustedSize;}];
}

- (void)showMealtimePickers
{
    [_breakfastTimeLabel setHidden:NO];
    [_breakfastTimePicker setHidden:NO];
    [_lunchTimeLabel setHidden:NO];
    [_lunchTimePicker setHidden:NO];
    [_dinnerTimeLabel setHidden:NO];
    [_dinnerTimePicker setHidden:NO];
    
    //self.scrollView.contentSize=self.contentView.bounds.size;
    
    //add animation
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionTransitionCurlDown animations:^{self.scrollView.contentSize=self.contentView.bounds.size;} completion:NULL];
    [self.scrollView setContentOffset:CGPointMake(0, ([self.scrollView contentOffset].y)+([[UIScreen mainScreen] bounds].size.height)/1.5) animated:YES]; //move the view down by 2/3 of the screen size
}

- (CGFloat)heightOfMealtimePickers
{
    CGFloat topY=[_breakfastTimeLabel frame].origin.y;
    CGFloat botomY=[_dinnerTimePicker frame].origin.y+[_dinnerTimePicker frame].size.height;
    
    return botomY-topY;
}

- (BOOL)isUserInputValid
{
    return ([self isNameValid] && [self isUsernameValid] && [self isPasswordValid] && [self isEmailValid] && [self isAgeValid] && [self isWeightValid]);
}

- (BOOL)isNameValid
{
    if ([_firstNameField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"atM-a7-zSX.placeholder", @"atM-a7-zSX.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_lastNameField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"GeS-5t-aAH.placeholder", @"GeS-5t-aAH.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isUsernameValid
{
    if ([_usernameField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"4bU-N2-4U1.placeholder", @"4bU-N2-4U1.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_usernameField.text length]>16 || [_usernameField.text length]<6)
    {
        [UINavigationController displayErrorPopupWithMessage:usernameLengthErrorMessage];
        return NO;
    }
    
    else if ([_usernameField.text rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:alphaNumeric] invertedSet]].location != NSNotFound) //check to make sure only alphanumeric characters are in the username
    {
        [UINavigationController displayErrorPopupWithMessage:usernameSpecialCharactersErrorMessage];
        return NO;
    }
    
    else if ([SimpleKeychain load:_usernameField.text]!=NULL)
    {
        [UINavigationController displayErrorPopupWithMessage:usernameAlreadyInUseErrorMessage];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isPasswordValid
{
    if ([_passwordField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"ZP-C8-o5C.placeholder", @"ZP-C8-o5C.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_passwordField.text length]>24 || [_passwordField.text length]<8)
    {
        [UINavigationController displayErrorPopupWithMessage:passwordLengthErrorMessage];
        return NO;
    }
    
    else if ([_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:lowerCaseChars]].location == NSNotFound || [_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:upperCaseChars]].location == NSNotFound || [_passwordField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:numbers]].location == NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:passwordFormatErrorMessage];
        return NO;
    }
    
    else if (![_passwordField.text isEqualToString:_repeatPasswordField.text])
    {
        [UINavigationController displayErrorPopupWithMessage:passwordsDontMatchErrorMessage];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isEmailValid
{
    if ([_emailField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"2KB-IB-r6t.placeholder", @"2KB-IB-r6t.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_emailField.text rangeOfString:@"@"].location == NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:emailNeedsAmpersandErrorMessage];
        return NO;
    }
    
    else if ([_emailField.text rangeOfString:@".com"].location == NSNotFound && [_emailField.text rangeOfString:@".net"].location == NSNotFound && [_emailField.text rangeOfString:@".org"].location == NSNotFound && [_emailField.text rangeOfString:@".gov"].location == NSNotFound && [_emailField.text rangeOfString:@".edu"].location == NSNotFound )
    {
        [UINavigationController displayErrorPopupWithMessage:emailNeedsDomainSuffixErrorMessage];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isAgeValid
{
    if ([_ageField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"0xi-6w-KIB.text", @"0xi-6w-KIB.text")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_ageField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:alpha]].location != NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:[[alphaError1 stringByAppendingString:NSLocalizedString(@"0xi-6w-KIB.text", @"0xi-6w-KIB.text")] stringByAppendingString: alphaError2]];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (BOOL)isWeightValid
{
    if ([_currentWeightField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"2QQ-TH-jru.text", @"2QQ-TH-jru.text")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_currentWeightField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:alpha]].location != NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:[[alphaError1 stringByAppendingString:NSLocalizedString(@"2QQ-TH-jru.text", @"2QQ-TH-jru.text")] stringByAppendingString: alphaError2]];
        return NO;
    }
    
    else if ([_goalWeightField.text isEqualToString:@""])
    {
        [UINavigationController displayErrorPopupWithMessage:[[registrationErrorMessage1 stringByAppendingString:NSLocalizedString(@"UcL-kz-MRU.placeholder", @"UcL-kz-MRU.placeholder")] stringByAppendingString: registrationErrorMessage2]];
        return NO;
    }
    
    else if ([_goalWeightField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:alpha]].location != NSNotFound)
    {
        [UINavigationController displayErrorPopupWithMessage:[[alphaError1 stringByAppendingString:NSLocalizedString(@"UcL-kz-MRU.placeholder", @"UcL-kz-MRU.placeholder")] stringByAppendingString: alphaError2]];
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (void)createNewAccountFromEneteredData
{
    NSManagedObjectContext* context=[self managedObjectContext];
    User* newAccount=[NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    [self createLoginDataForUsername:_usernameField.text];
    [self createPersonalDataForUser:newAccount inContext:context];
    
    NSError* error;
    if (![[self managedObjectContext] save:&error])
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:errorSavingData];
    }
}

- (void)createLoginDataForUsername:(NSString*)username
{
    [SimpleKeychain save:username data:_passwordField.text];
}

- (void)createPersonalDataForUser:(User*)user inContext:(NSManagedObjectContext*)context
{
    PersonalInformation* personalInfo=[NSEntityDescription insertNewObjectForEntityForName:@"PersonalInformation"inManagedObjectContext:context];
    personalInfo.username=_usernameField.text;
    personalInfo.age=[NSNumber numberWithInteger:[_ageField.text integerValue]];
    personalInfo.email=_emailField.text;
    personalInfo.firstName=_firstNameField.text;
    personalInfo.lastName=_lastNameField.text;
    personalInfo.goalWeight=[NSNumber numberWithInteger:[_goalWeightField.text integerValue]];
    NSNumber* initialWeight=[NSNumber numberWithInteger:[_currentWeightField.text integerValue]];
    personalInfo.initialWeight=initialWeight;
    [self setupProgressGraphWithInitialWeight:initialWeight forUser:user inContext: context];
    
    UserSettings* settings=[NSEntityDescription insertNewObjectForEntityForName:@"UserSettings" inManagedObjectContext:context];
    settings.autoReminders=[NSNumber numberWithBool:[_autoRemindersSwitch isOn]];
    settings.stayLoggedIn=[NSNumber numberWithBool:YES];
    settings.loggedOutReminders=[NSNumber numberWithBool:YES];
    
    if ([settings.autoReminders boolValue])
    {
        personalInfo.breakfastTime=[_breakfastTimePicker date];
        personalInfo.lunchTime=[_lunchTimePicker date];
        personalInfo.dinnerTime=[_dinnerTimePicker date];
        
        NSMutableArray* alarmsForCurrentUser=[[NSMutableArray alloc] init];
        
        AlarmObject* newAlarm=[[AlarmObject alloc] init];
        [newAlarm setLabel:breakfastLabel];
        [newAlarm setTimeToSetOff:[personalInfo breakfastTime]];
        [newAlarm setEnabled:YES];
        [newAlarm setNotificationID:[self getUniqueNotificationID]];
        [newAlarm setAlarmOwner:[personalInfo username]];
        
        [alarmsForCurrentUser addObject:newAlarm];

        newAlarm=[[AlarmObject alloc] init];
        [newAlarm setLabel:lunchLabel];
        [newAlarm setTimeToSetOff:[personalInfo lunchTime]];
        [newAlarm setEnabled:YES];
        [newAlarm setNotificationID:[self getUniqueNotificationID]];
        [newAlarm setAlarmOwner:[personalInfo username]];
        
        [alarmsForCurrentUser addObject:newAlarm];

        newAlarm=[[AlarmObject alloc] init];
        [newAlarm setLabel:dinnerLabel];
        [newAlarm setTimeToSetOff:[personalInfo dinnerTime]];
        [newAlarm setEnabled:YES];
        [newAlarm setNotificationID:[self getUniqueNotificationID]];
        [newAlarm setAlarmOwner:[personalInfo username]];
        
        [alarmsForCurrentUser addObject:newAlarm];
        
        personalInfo.alarmsForUser=alarmsForCurrentUser;
        
        [AlarmListTableController alarmsForCurrentUserWithManagedObjectContext:[self managedObjectContext]];
    }
    
    personalInfo.gender=_genderValue;
    NSString* feet=[[_heightPickerListFt objectAtIndex:[_feetRow integerValue]] string];
    NSString* inches=[[_heightPickerListIn objectAtIndex:[_inchesRow integerValue]] string];
    personalInfo.height=[feet stringByAppendingString:inches];
    
    user.info=personalInfo;
    personalInfo.user=user;
    user.userSettings=settings;
    settings.user=user;
}

- (void)setupProgressGraphWithInitialWeight:(NSNumber*)initialWeight forUser:(User*)user inContext:(NSManagedObjectContext*)context
{
    ProgressGraph* newGraph=[NSEntityDescription insertNewObjectForEntityForName:@"ProgressGraph" inManagedObjectContext:context];
    user.progressGraph=newGraph;
    newGraph.user=user;
    GraphNode* firstNode=[NSEntityDescription insertNewObjectForEntityForName:@"GraphNode" inManagedObjectContext:context];
    firstNode.weight=initialWeight;
    firstNode.date=[NSDate date];
    firstNode.parentGraphNew=newGraph;
    firstNode.parentGraphOld=newGraph;
    newGraph.newestNode=firstNode;
    newGraph.oldestNode=firstNode;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView layoutIfNeeded];
    [self hideMealtimePickers];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _activeField = nil;
}

- (void)registerForKeyBoardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _activeField.frame.origin))
    {
        [self.scrollView scrollRectToVisible:_activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)notfication
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}

- (BOOL)textFieldShouldReturn:(AdvancingUITextField *)textField
{
    //check if we are at the end, and if so (RETURNS YES) perform submit action as if it came from _submitbutton
    BOOL shouldReturn=[super textFieldShouldReturn:textField];
    if (shouldReturn)
    {
        if ([self shouldPerformSegueWithIdentifier:@"RegistrationAttempt" sender:_submitButton])
        {
            [self performSegueWithIdentifier:@"RegistrationAttempt" sender:_submitButton];
        }
    }
    
    return shouldReturn;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _genderPicker)
    {
        return 1;
    }
    
    else if (pickerView == _heightPicker)
    {
        return 2;
    }
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _genderPicker)
    {
        return [_genderPickerList count];
    }
    
    else //must be the height picker
    {
        if (component == 0)//first coulmn
        {
            return [_heightPickerListFt count];
        }
        
        else //second column
        {
            return [_heightPickerListIn count];
        }
    }
}

- (NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _genderPicker)
    {
        return [_genderPickerList objectAtIndex:row];
    }
    
    else //must be height picker
    {
        if (component == 0)//feet column
        {
            return [_heightPickerListFt objectAtIndex:row];
        }
        
        else //inches column
        {
            return [_heightPickerListIn objectAtIndex:row];
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _genderPicker)
    {
        _genderValue=[[_genderPickerList objectAtIndex:row] string];
        
    }
    
    else //must be the height picker
    {
        if (component == 0)//feet column
        {
            _feetRow=[NSNumber numberWithInt:row];
        }
        
        else //inches column
        {
            _inchesRow=[NSNumber numberWithInt:row];
        }
    }
}

//Get Unique Notification ID for a new alarm O(n)
-(int)getUniqueNotificationID
{
    NSMutableDictionary * hashDict = [[NSMutableDictionary alloc]init];
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSNumber *uid= [userInfoCurrent valueForKey:@"notificationID"];
        NSNumber * value =[NSNumber numberWithInt:1];
        [hashDict setObject:value forKey:uid];
    }
    for (int i=0; i<[eventArray count]+1; i++)
    {
        NSNumber * value = [hashDict objectForKey:[NSNumber numberWithInt:i]];
        if(!value)
        {
            return i;
        }
    }
    return 0;
    
}

@end
