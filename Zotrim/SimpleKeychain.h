//
//  SimpleKeychain.h
//  Zotrim
//
//  Created by Dexter Richard on 2/20/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleKeychain : NSObject

+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;
+ (void)deleteAll;
@end
