//
//  PersonalInformation.h
//  Zotrim
//
//  Created by Dexter Richard on 6/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface PersonalInformation : NSManagedObject

@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSDate * breakfastTime;
@property (nonatomic, retain) NSDate * dinnerTime;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSNumber * goalWeight;
@property (nonatomic, retain) NSString * height;
@property (nonatomic, retain) NSNumber * initialWeight;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSDate * lunchTime;
@property (nonatomic, retain) id alarmsForUser;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) User *user;

@end
