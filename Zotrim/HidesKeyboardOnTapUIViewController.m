//
//  HidesKeyboardOnTapUIViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/31/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HidesKeyboardOnTapUIViewController.h"

@interface HidesKeyboardOnTapUIViewController ()

@end

@implementation HidesKeyboardOnTapUIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self registerForTapNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerForTapNotifications
{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:singleTap];
}

+ (void)registerViewControllerForTapNotifications:(UIViewController *)controller
{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [controller.view addGestureRecognizer:singleTap];
}

- (void)resignOnTap:(id)sender
{
    [self.view endEditing:YES];
}

+ (void)viewController:(UIViewController*)controller resignOnTap:(id)sender
{
    [controller.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(AdvancingUITextField *)textField
{
    if ([textField shouldAdvance])
    {
        for (int i=0; i<[_textFields count];i++)
        {
            AdvancingUITextField* otherField=[_textFields objectAtIndex:i];
            if ([otherField isEqual:textField])
            {
                [textField resignFirstResponder];
                otherField=[_textFields objectAtIndex:i+1];
                [otherField becomeFirstResponder];
            }
        }
        
        return NO;
    }
    
    else //shouldn't advance, we are at the end of the form
    {
        [textField resignFirstResponder];
        return YES;
    }
}

@end
