//
//  SettingsViewControler.h
//  Zotrim
//
//  Created by Dexter Richard on 6/27/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "APLTableViewController.h"
#import "HidesKeyboardOnTapUIViewController.h"
#import "ConfigNumbers.h"
#import "FormattedPlaceholderTextField.h"
#import "UserSettings.h"
#import "SimpleKeychain.h"
#import "AlarmListTableController.h"
#import "AppDelegate.h"

@interface SettingsViewControler : APLTableViewController

@property (retain, nonatomic)HidesKeyboardOnTapUIViewController* mocVC;

@property (retain, nonatomic)UILabel* firstNameLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* firstNameField;
@property (retain, nonatomic)UILabel* lastNameLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* lastNameField;
@property (retain, nonatomic)UILabel* ageLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* ageField;

@property (retain, nonatomic)UILabel* passwordLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* passwordField;
@property (retain, nonatomic)UILabel* reEnterPasswordLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* reEnterPasswordField;
@property (retain, nonatomic)UILabel* emailLabel;
@property (retain, nonatomic)FormattedPlaceholderTextField* emailField;

@property (retain, nonatomic)UILabel* yesLabel;
@property (retain, nonatomic)UILabel* noLabel;

@property (retain, nonatomic)UILabel* autoLoginLabel;
@property (retain, nonatomic)UIView* autoLoginSwitchView;
@property (retain, nonatomic)UISwitch* autoLoginYesNoSwitch;
@property (strong)NSNumber* autoLogin;
@property (retain, nonatomic)UILabel* loggedOutRemindersLabel;
@property (retain, nonatomic)UIView* loggedOutRemindersSwitchView;
@property (retain, nonatomic)UISwitch* loggedOutRemindersYesNoSwitch;
@property NSNumber* loggedOutReminders;
@property (retain, nonatomic)UILabel* autoRemindersLabel;
@property (retain, nonatomic)UIView* autoRemindersSwitchView;
@property (retain, nonatomic)UISwitch* autoRemindersYesNoSwitch;
@property NSNumber* autoReminders;

@property (retain, nonatomic)UILabel* breakfastTimePickerLabel;
@property (retain, nonatomic)UIDatePicker* breakfastTimePicker;
@property (retain, nonatomic)UILabel* lunchTimePickerLabel;
@property (retain, nonatomic)UIDatePicker* lunchTimePicker;
@property (retain, nonatomic)UILabel* dinnerTimePickerLabel;
@property (retain, nonatomic)UIDatePicker* dinnerTimePicker;

- (IBAction)switchValueChanged:(id)sender;

- (NSManagedObjectContext*)managedObjectContext;
- (void)setManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;

@end
