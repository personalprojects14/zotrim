//
//  UserSettings.m
//  Zotrim
//
//  Created by Dexter Richard on 6/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "UserSettings.h"
#import "User.h"


@implementation UserSettings

@dynamic autoReminders;
@dynamic stayLoggedIn;
@dynamic loggedOutReminders;
@dynamic user;

@end
