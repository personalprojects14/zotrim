//
//  ConfigNumbers.c
//  Zotrim
//
//  Created by Dexter Richard on 6/9/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

const NSInteger minimumWeightLbs=80;
const NSInteger maximumWeightLbs=400;
const NSInteger firstDay=1;
const NSInteger firstMonth=1;
const NSInteger firstYear=2014;
const double graphBufferX=.0075;
const double graphBufferY=.07;
const NSInteger xAxisTickIntervalDivisor=7;
const double purpleColorRed=50.0/255.0;
const double purpleColorGreen=42.0/255.0;
const double purpleColorBlue=119.0/255.0;
const NSInteger initialGraphWhitespaceX=5;
const NSInteger initialGraphWhitespaceY=30;
const double animationFPS=1.0/1.5;
const int DEFAULT_ROW_HEIGHT=75;
const int PICKER_ROW_HEIGHT=260;