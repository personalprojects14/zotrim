//
//  FixedSizeQueue_MA.h
//  Zotrim
//
//  This implementation does not allow duplicates. Every item in the queue is unique. This is enforced in enqueue.
//
//  Created by Dexter Richard on 4/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "FixedSizeQueue.h"

@interface FixedSizeQueue_MA : FixedSizeQueue

@property (retain, nonatomic) NSNumber* maxSize;
@property (retain, nonatomic) NSMutableArray* contents;

//make this class able to recieve messages from NSMutableArray and NSArray so we can forward them to contents
//- (instancetype)initWithCapacity:(NSUInteger)numItems;
- (NSUInteger)count;
- (id)firstObject;
- (id)lastObject;
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (BOOL)containsObject:(id)anObject;
- (NSUInteger)indexOfObject:(id)anObject;
- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id *)stackbuf count:(NSUInteger)len;

@end
