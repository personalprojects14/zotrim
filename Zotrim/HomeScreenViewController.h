//
//  HomeScreenViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgotAccountViewController.h"
#import "LoginViewController.h"

#import "PersonalInformation.h"

@interface HomeScreenViewController : ManagedObjectContextViewController

- (IBAction)loginUnwindSegueAction:(UIStoryboardSegue*)segue;
- (IBAction)cancelUnwindSegueAction:(UIStoryboardSegue*)segue;
- (IBAction)recentsButtonWasPressed:(UIButton*)recentsButton;

- (void)userDidLogOut;

- (void)displayRecents;

@property IBOutlet UILabel* recents;

@property (weak, nonatomic) IBOutlet UIButton *recentsLabel1;
@property (weak, nonatomic) IBOutlet UIButton *recentsLabel2;
@property (weak, nonatomic) IBOutlet UIButton *recentsLabel3;
@property (weak, nonatomic) IBOutlet UIButton *recentsLabel4;

@end

