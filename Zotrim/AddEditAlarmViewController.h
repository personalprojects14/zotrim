//
//  AddAlarmViewController.h
//  AlarmClock
//


#import <UIKit/UIKit.h>

#import "AlarmLabelEditViewController.h"
#import "AlarmObject.h"
#import "ManagedObjectContextAlarmController.h"
#import "PersonalInformation.h"
#import "User.h"
#import "StateInformation.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "ConfigStrings.h"

@interface AddEditAlarmViewController :  ManagedObjectContextAlarmController<AlarmLabelEditViewControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIDatePicker *timeToSetOff;
@property (nonatomic, strong) IBOutlet UINavigationItem *navItem;
@property (nonatomic, assign) NSInteger indexOfAlarmToEdit;
@property(atomic,strong) NSString *label;
@property(nonatomic,assign) BOOL editMode;
@property(nonatomic,assign) int notificationID;

+ (void)scheduleLocalNotificationWithDate:(NSDate *)fireDate
                                  atIndex:(int)indexOfObject withManagedObjectContext:(NSManagedObjectContext*) managedObjectContext;

+ (void)cancelExistingNotificationWithID:(int)notificationID;

@end
