//
//  LoginInformation.h
//  Zotrim
//
//  Created by Dexter Richard on 2/20/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SimpleKeychain.h"


@interface LoginInformation : NSManagedObject

@property (nonatomic, retain) SimpleKeychain* loginInfoDictionary;

@end
