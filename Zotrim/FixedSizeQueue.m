#import "FixedSizeQueue.h"
#import "FixedSizeQueue_MA.h"

@implementation FixedSizeQueue

- (instancetype)initWithMaxSize:(NSNumber*)maxSize
{
    return [[FixedSizeQueue_MA alloc] initWithMaxSize:maxSize];
}

@end