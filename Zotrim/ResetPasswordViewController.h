//
//  ResetPasswordViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 2/2/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HidesKeyboardOnTapUIViewController.h"
#import "FormattedPlaceholderTextField.h"
#import "UINavigationController+SimpleErrorMessage.h"

#import "ConfigStrings.h"
#import "SimpleKeychain.h"

@interface ResetPasswordViewController : HidesKeyboardOnTapUIViewController

@property (weak,nonatomic) IBOutlet FormattedPlaceholderTextField* nPasswordField;
@property (weak,nonatomic) IBOutlet FormattedPlaceholderTextField* confirmPasswordField;

@property (weak, nonatomic) IBOutlet UILabel *usernameText;

@property (retain, nonatomic) NSString* username;

@end
