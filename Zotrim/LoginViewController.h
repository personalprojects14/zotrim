//
//  LoginScreenViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/31/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "HidesKeyboardOnTapUIViewController.h"
#import "FormattedPlaceholderTextField.h"
#import "SimpleKeychain.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "ConfigStrings.h"
#import "FixedSizeQueue.h"

#import "StateInformation.h"
#import "User.h"
#import "PersonalInformation.h"
#import "UserSettings.h"

@interface LoginViewController : HidesKeyboardOnTapUIViewController

@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField *usernameField;
@property (weak, nonatomic) IBOutlet FormattedPlaceholderTextField *passwordField;

@property (retain, nonatomic) NSString* username;

- (IBAction)cancelToLoginUnwindSegueAction:(UIStoryboardSegue*)segue;

- (void)userDidLogin;
- (void)autofill:(NSString*)username;

@end
