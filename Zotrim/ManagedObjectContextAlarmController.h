//
//  ManagedObjectContextAlarmController.h
//  Zotrim
//
//  Created by Dexter Richard on 6/25/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ManagedObjectContextViewController.h"

@interface ManagedObjectContextAlarmController : ManagedObjectContextViewController

+(int)getUniqueNotificationID;

@end
