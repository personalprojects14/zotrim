//
//  HidesKeyboardOnTapUIViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 1/31/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvancingUITextField.h"
#import "ManagedObjectContextViewController.h"

@interface HidesKeyboardOnTapUIViewController : ManagedObjectContextViewController<UITextFieldDelegate>

@property (strong, nonatomic) NSArray* textFields;

+ (void)registerViewControllerForTapNotifications:(UIViewController*)controller;
+ (void)viewController:(UIViewController*)controller resignOnTap:(id)sender;

- (BOOL)textFieldShouldReturn:(AdvancingUITextField*)textField;

@end
