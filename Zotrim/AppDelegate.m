//
//  AppDelegate.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Clear keychain on first run in case of reinstallation
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRun"]) {
        // Delete values from keychain here
        [SimpleKeychain deleteAll];
    
        [[NSUserDefaults standardUserDefaults] setValue:@"1strun" forKey:@"FirstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //cancel any local notifications a previous version or installation may have set up 
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    
    // Override point for customization after application launch.
    [self loadManagedObjectContextFromDocument];
    
    UILocalNotification* notification=[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification)
    {
        [self application:[UIApplication sharedApplication] didReceiveLocalNotification:notification];
    }
        
    return YES;
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
    HomeScreenViewController* controller = (HomeScreenViewController*)navigationController.topViewController;
    NSManagedObjectContext* context=[controller managedObjectContext];
    
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:context];
    StateInformation* stateInfo=[ManagedObjectContextViewController loadCurrentStateInfoFromManagedObjectContext:context];
    NSString* username=[[notification userInfo] objectForKey:@"username"];
    
    if ([username isEqualToString:[stateInfo currentUsername]] && [stateInfo loggedIn])
    {
        NSString* usersFirstName=[[notification userInfo] objectForKey:@"usersFirstName"];
        UIAlertView *alarmAlert = [[UIAlertView alloc] initWithTitle:@"Supplement Reminder"
                                                             message:[usersFirstName stringByAppendingString:NSLocalizedString(@"ALERT_TEXT", @"Alert Text")]
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ALERT_OKAY", @"Alarm Okay")
                                                   otherButtonTitles:nil, nil];
        [alarmAlert show];
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
            SystemSoundID soundID;
            NSURL* url=[NSURL URLWithString:@"/System/Library/Audio/UISounds/New/Fanfare.caf"];
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundID);
            AudioServicesPlayAlertSound(soundID);
        }
    }
    
    else if ([settings loggedOutReminders])
    {
        NSString* usersFirstName=[[notification userInfo] objectForKey:@"usersFirstName"];
        UIAlertView *alarmAlert = [[UIAlertView alloc] initWithTitle:@"Supplement Reminder"
                                                             message:[usersFirstName stringByAppendingString:NSLocalizedString(@"ALERT_TEXT", @"Alert Text")]
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ALERT_OKAY", @"Alarm Okay")
                                                   otherButtonTitles:nil, nil];
        [alarmAlert show];
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
            SystemSoundID soundID;
            NSURL* url=[NSURL URLWithString:@"/System/Library/Audio/UISounds/New/Fanfare.caf"];
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundID);
            AudioServicesPlayAlertSound(soundID);
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)loadManagedObjectContextFromDocument
{
    [self loadOrCreateManagedDocument];
}

- (UIManagedDocument*)loadOrCreateManagedDocument
{
    NSURL *docURL = [[self applicationDocumentsDirectoryRoot] URLByAppendingPathComponent:applicationDocumentsDirectory];
    UIManagedDocument* doc = [[UIManagedDocument alloc] initWithFileURL:docURL];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    doc.persistentStoreOptions = options;
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[docURL path]])
    {
        [doc openWithCompletionHandler:^(BOOL success)
            {
                if (!success)
                {
                    // Handle the error.
                }
                
                else //success
                {
                    [self documentIsReady:doc];
                }
            }
        ];
    }
    else
    {
        [doc saveToURL:docURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
            {
                if (!success)
                {
                    // Handle the error.
                }
                
                else //success
                {
                    [self documentIsReady:doc];
                }
            }
        ];
    }
    
    return doc;
}

- (void)documentIsReady:(UIManagedDocument*)document
{
    if (document.documentState == UIDocumentStateNormal)
    {
        [self setupApplicationManagedObjectContext:[document managedObjectContext]];
    }
    
    if (!_managedObjectContext)
    {
        //Handle the error.
    }
}

- (void)setupApplicationManagedObjectContext:(NSManagedObjectContext*) managedObjectContext
{
    _managedObjectContext=managedObjectContext;
    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
    HomeScreenViewController* controller = (HomeScreenViewController*)navigationController.topViewController;//should be HomeScreenVC
    controller.managedObjectContext= _managedObjectContext;
    [controller viewDidAppear:NO];
}

- (NSURL *)applicationDocumentsDirectoryRoot
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (void)refreshViews
{
    NSArray *windows = [UIApplication sharedApplication].windows;
    for (UIWindow *window in windows) {
        for (UIView *view in window.subviews) {
            [view removeFromSuperview];
            [window addSubview:view];
        }
    }
}

@end
