//
//  UserDocument.h
//  Zotrim
//
//  Created by Dexter Richard on 2/3/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UsersDocument : UIManagedDocument

@end
