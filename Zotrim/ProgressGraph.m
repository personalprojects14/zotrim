//
//  ProgressGraph.m
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ProgressGraph.h"
#import "GraphNode.h"
#import "User.h"


@implementation ProgressGraph

@dynamic oldestNode;
@dynamic user;
@dynamic newestNode;

@end
