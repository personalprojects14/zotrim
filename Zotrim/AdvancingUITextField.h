//
//  AdvancingUITextField.h
//  Zotrim
//
//  Created by Dexter Richard on 2/1/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvancingUITextField : UITextField

@property (assign,atomic) BOOL shouldAdvance;

@end
