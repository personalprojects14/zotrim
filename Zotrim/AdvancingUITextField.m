//
//  AdvancingUITextField.m
//  Zotrim
//
//  Created by Dexter Richard on 2/1/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "AdvancingUITextField.h"

@implementation AdvancingUITextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
