//
//  ProgressGraph.h
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GraphNode, User;

@interface ProgressGraph : NSManagedObject

@property (nonatomic, retain) GraphNode *oldestNode;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) GraphNode *newestNode;

@end
