//
//  SecondViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "ProgressTrackerViewController.h"

@interface ProgressTrackerViewController ()

@end

@implementation ProgressTrackerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setupView];
    [self setupData];
    [self setupGraph];
    [self setupPickers];
}

- (void)setupView
{
    CGSize adjustedSize=self.contentView.bounds.size;
    self.scrollView.contentSize=adjustedSize;
}

- (void)setupGraph
{
    //graph
    CPTXYGraph* graph=[[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [graph applyTheme:theme];
    graph.frame=_graphHost.bounds;
    graph.paddingRight                = 0;
    graph.paddingLeft                 = 10;
    graph.paddingTop                  = 0;
    graph.paddingBottom               = 20;
    graph.plotAreaFrame.masksToBorder = NO;
    graph.plotAreaFrame.cornerRadius  = 0.0;
    graph.plotAreaFrame.borderLineStyle=NULL;
    
    // Axes
    CPTXYAxisSet *xyAxisSet        = (id)graph.axisSet;
    CPTXYAxis *xAxis               = xyAxisSet.xAxis;
    CPTMutableLineStyle *lineStyle = [xAxis.axisLineStyle mutableCopy];
    lineStyle.lineCap   = kCGLineCapButt;
    xAxis.axisLineStyle = lineStyle;
    xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    CPTMutableTextStyle* xAxisStyle=[[CPTMutableTextStyle alloc] init];
    [xAxisStyle setColor:[CPTColor colorWithCGColor:[[UIColor colorWithRed:purpleColorRed green:purpleColorGreen blue:purpleColorBlue alpha:1.0] CGColor]]];
    xAxis.labelTextStyle=xAxisStyle;
    
    CPTXYAxis *yAxis = xyAxisSet.yAxis;
    yAxis.axisLineStyle = kCGLineCapButt;
    yAxis.axisLineStyle = lineStyle;
    yAxis.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    CPTMutableTextStyle* yAxisStyle=[[CPTMutableTextStyle alloc] init];
    [yAxisStyle setColor:[CPTColor colorWithCGColor:[[UIColor colorWithRed:purpleColorRed green:purpleColorGreen blue:purpleColorBlue alpha:1.0] CGColor]]];
    yAxis.labelTextStyle=yAxisStyle;
    yAxis.majorIntervalLength=CPTDecimalFromInt(10);
    yAxis.labelFormatter = formatter;
    yAxis.labelOffset = -4.0;
    
    // Line plot
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] initWithFrame:graph.bounds];
    dataSourceLinePlot.identifier     = @"MainPlot";
    CPTLineStyle* graphLineStyle = [CPTLineStyle lineStyle];//this may need to me mutable and changed
    //CPTMutableLineStyle *graphLineStyle=[CPTMutableLineStyle lineStyle];
    dataSourceLinePlot.dataLineStyle  = graphLineStyle;
    dataSourceLinePlot.dataSource     = self;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDecimal;
    CPTPlotSymbol* plotPoint=[CPTPlotSymbol ellipsePlotSymbol];
    dataSourceLinePlot.plotSymbol=plotPoint;
    dataSourceLinePlot.labelOffset=5;
    [graph addPlot:dataSourceLinePlot];
    _plot=dataSourceLinePlot;
    
    CPTScatterPlot* goalPlot=[[CPTScatterPlot alloc] initWithFrame:graph.bounds];
    goalPlot.identifier=@"GoalPlot";
    CPTMutableLineStyle* goalLineStyle = [CPTMutableLineStyle lineStyle];
    [goalLineStyle setLineColor:[CPTColor colorWithCGColor:[[UIColor greenColor] CGColor]]];
    goalPlot.dataLineStyle=goalLineStyle;
    goalPlot.dataSource=self;
    dataSourceLinePlot.cachePrecision=CPTPlotCachePrecisionDecimal;
    [graph addPlot:goalPlot];
    
    CABasicAnimation *fadeInAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeInAnimation.duration            = 2.0;
    fadeInAnimation.removedOnCompletion = YES;
    fadeInAnimation.fillMode            = kCAFillModeForwards;
    fadeInAnimation.fromValue           = @0.0;
    fadeInAnimation.toValue             = @1.0;
    [graph addAnimation:fadeInAnimation forKey: @"animateOpacity"];
    
    self.graphHost.hostedGraph=graph;
    
    [self setupAxes];
}

- (void)setupAxes
{
    [self fitGraphAxesToData];
    
    CPTXYAxisSet* axes=(CPTXYAxisSet*)self.graphHost.hostedGraph.axisSet;
    CPTXYAxis* xAxis=axes.xAxis;
    
    NSArray* locationsAndLabels=[self createTicksandLabelsForXAxis];
    xAxis.majorTickLocations=(NSMutableSet*)[locationsAndLabels objectAtIndex:0];
    xAxis.axisLabels=(NSMutableSet*)[locationsAndLabels objectAtIndex:1];
    [xAxis updateMajorTickLabels];
    [axes.yAxis updateMajorTickLabels];
}

- (void)fitGraphAxesToData
{
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)self.graphHost.hostedGraph.defaultPlotSpace;
    CPTXYAxisSet* axes=(CPTXYAxisSet*)self.graphHost.hostedGraph.axisSet;
    
    if (([_graphData count] / 2) == 1)//only one data point is a special case
    {
        int pointX=[[_graphData objectAtIndex:0] integerValue];
        int pointY=[[_graphData objectAtIndex:1] integerValue];
        
        int newMinX=pointX - initialGraphWhitespaceX;
        int newLenX=(2 * initialGraphWhitespaceX);
        
        CPTPlotRange* newRange=[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(newMinX) length:CPTDecimalFromInt(newLenX)];
        [CPTAnimation animate:plotSpace property:@"xRange" fromPlotRange:plotSpace.xRange toPlotRange:newRange duration:animationFPS];
        [plotSpace setXRange:newRange];
        
        int goalWeight=[(NSNumber*)[_goalData objectAtIndex:1]integerValue];
        int yLengthAdjustment=0;
        if (goalWeight < pointY)
        {
            yLengthAdjustment=pointY - goalWeight;
            pointY=goalWeight;
        }
        
        int newMinY=pointY - initialGraphWhitespaceY;
        int newLenY=yLengthAdjustment + (2 * initialGraphWhitespaceY);
        
        newRange=[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(newMinY) length:CPTDecimalFromInt(newLenY)];
        [CPTAnimation animate:plotSpace property:@"yRange" fromPlotRange:plotSpace.yRange toPlotRange:newRange duration:animationFPS];
        
        [CPTAnimation animate:axes.xAxis property:@"orthogonalCoordinateDecimal" fromDecimal:axes.xAxis.orthogonalCoordinateDecimal toDecimal:CPTDecimalFromInt(newMinY) duration:animationFPS];
        [CPTAnimation animate:axes.yAxis property:@"orthogonalCoordinateDecimal" fromDecimal:axes.yAxis.orthogonalCoordinateDecimal toDecimal:CPTDecimalFromInt(newMinX) duration:animationFPS];
    }
    
    else
    {
        [plotSpace scaleToFitPlots:[NSArray arrayWithObject:_plot]];
        int oldMin=[[NSDecimalNumber decimalNumberWithDecimal:plotSpace.xRange.minLimit] integerValue];
        int newMinX=oldMin - (int)(oldMin * graphBufferX);
        int oldLen=[[NSDecimalNumber decimalNumberWithDecimal:plotSpace.xRange.length]integerValue];
        int newXRange=oldLen + (2 * (int)(oldMin * graphBufferX));
        CPTPlotRange* newRange=[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(newMinX) length:CPTDecimalFromInt(newXRange)];
        [CPTAnimation animate:plotSpace property:@"xRange" fromPlotRange:plotSpace.xRange toPlotRange:newRange duration:animationFPS animationCurve:CPTAnimationCurveSinusoidalIn delegate:NULL];
        [plotSpace setXRange:newRange];
        
        oldMin=[[NSDecimalNumber decimalNumberWithDecimal:plotSpace.yRange.minLimit] integerValue];
        
        int goalWeight=[(NSNumber*)[_goalData objectAtIndex:1]integerValue];
        int yLengthAdjustment=0;
        if (goalWeight < oldMin)
        {
            yLengthAdjustment=oldMin - goalWeight;
            oldMin=goalWeight;
        }
        
        int newMinY=oldMin - (oldMin * graphBufferY);
        oldLen=[[NSDecimalNumber decimalNumberWithDecimal:plotSpace.yRange.length] integerValue];
        int newYRange=oldLen + (2 * (oldMin * graphBufferY)) + yLengthAdjustment;
        newRange=[CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(newMinY) length:CPTDecimalFromInt(newYRange)];
        [CPTAnimation animate:plotSpace property:@"yRange" fromPlotRange:plotSpace.yRange toPlotRange:newRange duration:animationFPS animationCurve:CPTAnimationCurveSinusoidalIn delegate:NULL];
        
        [CPTAnimation animate:axes.xAxis property:@"orthogonalCoordinateDecimal" fromDecimal:axes.xAxis.orthogonalCoordinateDecimal toDecimal:CPTDecimalFromInt(newMinY) duration:animationFPS animationCurve:CPTAnimationCurveLinear delegate:NULL];
        [CPTAnimation animate:axes.yAxis property:@"orthogonalCoordinateDecimal" fromDecimal:axes.yAxis.orthogonalCoordinateDecimal toDecimal:CPTDecimalFromInt(newMinX) duration:animationFPS animationCurve:CPTAnimationCurveLinear delegate:NULL];
    }
}

- (NSArray*)createTicksandLabelsForXAxis
{
    CPTPlotRange* xRange=[(CPTXYPlotSpace *)self.graphHost.hostedGraph.defaultPlotSpace xRange];
    CPTXYAxisSet* axisSet=(CPTXYAxisSet*)self.graphHost.hostedGraph.axisSet;
    NSInteger minDateNum=[[NSDecimalNumber decimalNumberWithDecimal:[xRange location]] integerValue];
    NSInteger maxDateNum=[[[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInt(minDateNum)] decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[xRange length]]] integerValue];
    
    NSInteger totalDays=maxDateNum-minDateNum;
    NSInteger interval=(totalDays + xAxisTickIntervalDivisor -1) / xAxisTickIntervalDivisor;
    
    NSMutableSet* majorLocations=[NSMutableSet set];
    NSMutableSet* xLabels=[NSMutableSet set];
    NSCalendar* calendar=[NSCalendar currentCalendar];
    
    for (int i=minDateNum; i < maxDateNum; i+=interval)
    {
        NSDateComponents* components=[calendar components:NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[self dateFromDaysSinceFirstDay:i]];
        NSString* dateLabel=[[[NSString stringWithFormat:@"%ld", (long)[components month]] stringByAppendingString:@"/"] stringByAppendingString:[NSString stringWithFormat:@"%ld", (long)[components day]]];
        CPTAxisLabel* label=[[CPTAxisLabel alloc] initWithText:dateLabel textStyle:axisSet.xAxis.labelTextStyle];
        [xLabels addObject:label];
        [label setOffset:axisSet.xAxis.majorTickLength];
        [label setTickLocation:CPTDecimalFromInt(i)];
        
        [majorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInt(i)]];
    }
    
    NSArray* locationsAndLabels=[[NSArray alloc] initWithObjects:majorLocations, xLabels, nil];
    
    return locationsAndLabels;
}

- (void)setupData
{
    [self setupGraphData];
    
    PersonalInformation* info=[ManagedObjectContextViewController loadCurrentUserInfoFromManagedObjectContext:[self managedObjectContext]];
    NSArray* goalPoints=[NSArray arrayWithObjects:[NSNumber numberWithInt:-100], [info goalWeight], [NSNumber numberWithInt:INT16_MAX], [info goalWeight], nil];
    _goalData=goalPoints;
}

- (void)setupGraphData
{
    NSDate* aMonthAgo;
    NSCalendar* calendar=[NSCalendar currentCalendar];
    NSDateComponents* components=[calendar components:NSMonthCalendarUnit | NSDayCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    [components setMonth:[components month]-1];
    aMonthAgo=[calendar dateFromComponents:components];
    
    _graphData=[self dataPointsFromDate:aMonthAgo toDate:[NSDate date]];
}

- (void)updateGraphData
{
    _graphData=[self dataPointsFromDate:[_fromDatePicker date] toDate:[_toDatePicker date]];
}

- (void)setupPickers
{
    [self setupDatePickers];
    [self setupWeightPicker];
}

- (void)setupDatePickers
{
    self.progressDatePicker.dateFormatTemplate=@"MMMd";
    self.toDatePicker.dateFormatTemplate=@"MMMdyy";
    self.fromDatePicker.dateFormatTemplate=@"MMMdyy";
    
    //preset to and from to the currently displayed range
    CPTXYPlotSpace* plotSpace=(CPTXYPlotSpace*)[self.graphHost.hostedGraph defaultPlotSpace];
    CPTPlotRange* xAxisRange=[plotSpace xRange];
    NSDate* minDate=[self dateFromDaysSinceFirstDay:[[NSDecimalNumber decimalNumberWithDecimal:[xAxisRange location]] integerValue]];
    NSDate* maxDate=[self dateFromDaysSinceFirstDay:[[[NSDecimalNumber decimalNumberWithDecimal:[xAxisRange location]] decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[xAxisRange length]]] integerValue]];
    
    [self.fromDatePicker setDate:minDate];
    [self.toDatePicker setDate:maxDate];
    [self.fromDatePicker setDateDelegate:self];
    [self.toDatePicker setDateDelegate:self];
}

- (void)setupWeightPicker
{
    _weightsList=[[NSMutableArray alloc] init];
    
    for (int i=minimumWeightLbs; i <=maximumWeightLbs; i++)
    {
        [_weightsList addObject:[NSNumber numberWithInt:i]];
    }
    
    int currentWeight=[[[[[ManagedObjectContextViewController loadCurrentUserFromManagedObjectContext:[self managedObjectContext]] progressGraph] newestNode]weight] integerValue];
    [_weightPicker selectRow:currentWeight - minimumWeightLbs inComponent:0 animated:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_weightsList count];
}

- (NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[NSAttributedString alloc] initWithString: [[_weightsList objectAtIndex:row] stringValue]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _weightSelection=[[_weightsList objectAtIndex:row] integerValue];
}

- (void)datePicker:(PMEDatePicker *)datePicker didSelectDate:(NSDate *)date
{
    if (datePicker == _fromDatePicker || datePicker == _toDatePicker)
    {
        CABasicAnimation *fadeInAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeInAnimation.duration            = 4.0;
        fadeInAnimation.removedOnCompletion = YES;
        fadeInAnimation.fillMode            = kCAFillModeForwards;
        fadeInAnimation.fromValue           = @0.0;
        fadeInAnimation.toValue             = @1.0;
        [self.graphHost.hostedGraph addAnimation:fadeInAnimation forKey: @"animateOpacity"];
        
        [self updateGraphData];
        [self.graphHost.hostedGraph reloadData];
        [self setupAxes];
    }
}

- (void)userDidAddProgress:(UIButton *)sender
{
    NSDate* date=[_progressDatePicker date];
    NSCalendar* calendar=[NSCalendar currentCalendar];
    NSDateComponents* components=[calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    [components setYear:[[calendar components:NSYearCalendarUnit fromDate:[NSDate date]] year]];
    date=[calendar dateFromComponents:components];
    
    NSNumber* weight=[_weightsList objectAtIndex:[_weightPicker selectedRowInComponent:0]];
    
    User* user=[ManagedObjectContextViewController loadCurrentUserFromManagedObjectContext:[self managedObjectContext]];
    ProgressGraph* graph=[user progressGraph];
    GraphNode* newestNode=[graph newestNode];
    GraphNode* nodeToAdd=[NSEntityDescription insertNewObjectForEntityForName:@"GraphNode" inManagedObjectContext:[self managedObjectContext]];
    [nodeToAdd setDate:date];
    [nodeToAdd setWeight:weight];
    
    GraphNode* node=newestNode;
    
    while (node != NULL && [[nodeToAdd date] compare:[node date]] == NSOrderedAscending)
    {
        node=[node previousNode];
    }
    
    if (node == NULL)
    {
        //new node is the oldest
        GraphNode* oldestNode=[graph oldestNode];
        [nodeToAdd setNextNode:oldestNode];
        [oldestNode setPreviousNode:nodeToAdd];
        [oldestNode setParentGraphOld:NULL];
        [nodeToAdd setParentGraphOld:graph];
        [graph setOldestNode:nodeToAdd];
    }
    
    else if ([[nodeToAdd date] compare:[node date]] == NSOrderedSame)
    {
        //new node needs to replace existing node
        [nodeToAdd setPreviousNode:[node previousNode]];
        [nodeToAdd setNextNode:[node nextNode]];
        [[node previousNode] setNextNode:nodeToAdd];
        [[node nextNode] setPreviousNode:nodeToAdd];
    }
    
    else if ([node nextNode] == NULL)
    {
        //new node is the newest
        [node setParentGraphNew:NULL];
        [nodeToAdd setParentGraphNew:graph];
        [graph setNewestNode:nodeToAdd];
        [node setNextNode:nodeToAdd];
        [nodeToAdd setPreviousNode:node];
    }
    
    else
    {
        //new node is inserted in the middle somewhere
        [nodeToAdd setNextNode:[node nextNode]];
        [nodeToAdd setPreviousNode:node];
        [node setNextNode:nodeToAdd];
    }

    NSError* error;
    if (![[self managedObjectContext] save:&error])
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:errorSavingData];
    }
    
    else
    {
        CABasicAnimation *fadeInAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeInAnimation.duration            = 4.0;
        fadeInAnimation.removedOnCompletion = YES;
        fadeInAnimation.fillMode            = kCAFillModeForwards;
        fadeInAnimation.fromValue           = @0.0;
        fadeInAnimation.toValue             = @1.0;
        [self.graphHost.hostedGraph addAnimation:fadeInAnimation forKey: @"animateOpacity"];
    
        [self updateGraphData];
        [self.graphHost.hostedGraph reloadData];
        [self setupAxes];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if ([(NSString*)[plot identifier] isEqualToString:@"MainPlot"])
    {
        return [_graphData count]/2;
    }
    
    else //must be goal plot
    {
        return 2;
    }
}

- (CPTNumericData*)dataForPlot:(CPTPlot *)plot recordIndexRange:(NSRange)indexRange
{
    if ([(NSString*)[plot identifier] isEqualToString:@"MainPlot"])
    {
        NSArray* dataArray=_graphData;
        if ([dataArray count] == 0)
        {
            return NULL;
        }
        
        else
        {
            CPTNumericData* data=[[CPTNumericData alloc] initWithArray:dataArray dataType:plot.decimalDataType shape:[NSArray arrayWithObjects:[NSNumber numberWithLong:[dataArray count]/2], [NSNumber numberWithInt:2],nil] dataOrder:CPTDataOrderRowsFirst];
            
            return data;
        }
 
    }
    
    else //must be goal line plot
    {
        CPTNumericData* data=[[CPTNumericData alloc] initWithArray:_goalData dataType:plot.decimalDataType shape:[NSArray arrayWithObjects:[NSNumber numberWithLong:2],[NSNumber numberWithInt:2], nil] dataOrder:CPTDataOrderRowsFirst];
        
        return  data;
    }
}

- (NSArray*)dataPointsFromDate:(NSDate*)fromDate toDate:(NSDate*)toDate
{
    NSMutableArray* dataPoints=[[NSMutableArray alloc] init];
    ProgressGraph* graph=[self loadCurrentUserGraphFromManagedObjectContext:[self managedObjectContext]];
    GraphNode* node=[graph newestNode];
    
    while (node != NULL && [[node date] compare:fromDate] != NSOrderedAscending)
    {
        if ([[node date] compare:toDate] != NSOrderedDescending)
        {
            [dataPoints addObject:[NSNumber numberWithLong:[self daysSinceFirstDate:[node date]]]];
            
            [dataPoints addObject:[node weight]];
        }
        
        node=[node previousNode];
    }
    
 // TEST DATA--------------------------------------------
//    [dataPoints addObject:[NSNumber numberWithInt:162]];
//    [dataPoints addObject:[NSNumber numberWithInt:230]];
//    [dataPoints addObject:[NSNumber numberWithInt:165]];
//    [dataPoints addObject:[NSNumber numberWithInt:228]];
//    [dataPoints addObject:[NSNumber numberWithInt:173]];
//    [dataPoints addObject:[NSNumber numberWithInt:222]];
    
    return dataPoints;
}

- (NSArray*)dataLabelsForPlot:(CPTPlot *)plot recordIndexRange:(NSRange)indexRange
{
    if ([(NSString*)[plot identifier] isEqualToString:@"MainPlot"])
    {
        CPTMutableTextStyle* redStyle=[[CPTMutableTextStyle alloc] init];
        [redStyle setColor:[CPTColor colorWithCGColor:[[UIColor redColor] CGColor]]];
        CPTMutableTextStyle* greenStyle=[[CPTMutableTextStyle alloc] init];
        [greenStyle setColor:[CPTColor colorWithCGColor:[[UIColor greenColor] CGColor]]];
        
        NSMutableArray* labels=[[NSMutableArray alloc] init];
        
        if ([_graphData count]/2 == 1)//only 1 node is a special case, just return 1 red label
        {
            [labels addObject:[[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%d", [[_graphData objectAtIndex:1] intValue]] style:redStyle]];
        }
    
        else
        {
            //TODO: Dont return every label when many points on screen (large range)
            int startPos=((indexRange.location * 2) + 1);
            NSNumber* previousWeight=(NSNumber*)[_graphData objectAtIndex:startPos+2];
            for (int i=startPos; i < ((indexRange.length * 2) + startPos)-2; i+=2)
            {
                previousWeight=(NSNumber*)[_graphData objectAtIndex:i+2];
                NSNumber* currentWeight=(NSNumber*)[_graphData objectAtIndex:i];
                
                if ([currentWeight compare:previousWeight] == NSOrderedDescending)//current weight is higher
                {
                    [labels addObject:[[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%d", [currentWeight intValue]] style:redStyle]];
                }
                
                else
                {
                    [labels addObject:[[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%d", [currentWeight intValue]] style:greenStyle]];
                }
            }
            
            //last item in the list (leftmost node) is a special case
            NSNumber* currentWeight=(NSNumber*)[_graphData objectAtIndex:[_graphData count] -1];
            [labels addObject:[[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%d", [currentWeight intValue]] style:redStyle]];
        }
        
        return labels;
    }
    
    else
    {
        return NULL;
    }
}

- (ProgressGraph*)loadCurrentUserGraphFromManagedObjectContext:(NSManagedObjectContext*)context
{
    User* user=[ManagedObjectContextViewController loadCurrentUserFromManagedObjectContext:context];
    
    return [user progressGraph];
}

- (NSInteger)daysSinceFirstDate:(NSDate*)recentDate
{
    NSDateComponents* firstDayComponents=[[NSDateComponents alloc] init];
    [firstDayComponents setDay:firstDay];
    [firstDayComponents setMonth:firstMonth];
    [firstDayComponents setYear:firstYear];
    NSCalendar* calendar=[NSCalendar currentCalendar];
    NSDate* firstDay=[calendar dateFromComponents:firstDayComponents];
    NSDateComponents* difference=[calendar components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:firstDay toDate:recentDate options:0];
    
    NSInteger dateNumber=[difference day] + ([difference month] * 30) + ([difference year] * 365);
    
    return dateNumber;
}

- (NSDate*)dateFromDaysSinceFirstDay:(NSUInteger)days
{
    //Todo: account for leap years
    int yearsSince=days/365;
    days=days%365;
    int monthsSince=0;
    
    if (days >= 31)
    {
        days-=31;
        monthsSince++;
    }
    
    if (days >= 28)
    {
        days-=28;
        monthsSince++;
    }
    
    BOOL thirtyDayMonth=NO;
    while ((days >= 30 && thirtyDayMonth) || (days >= 31 && !thirtyDayMonth))
    {
        if (thirtyDayMonth)
        {
            days-=30;
            monthsSince++;
            thirtyDayMonth=NO;
        }
        
        else
        {
            days-=31;
            monthsSince++;
            thirtyDayMonth=YES;
        }
    }
    
    int daysSince=days+1;
    int years=firstYear+yearsSince;
    int months=firstMonth+monthsSince;
    int totalDays=firstDay+daysSince;
    
    NSDateComponents* recentDayComponents=[[NSDateComponents alloc] init];
    [recentDayComponents setYear:years];
    [recentDayComponents setMonth:months];
    [recentDayComponents setDay:totalDays];
    NSCalendar* calendar=[NSCalendar currentCalendar];
    NSDate* recentDate=[calendar dateFromComponents:recentDayComponents];
    
    return recentDate;
}

@end
