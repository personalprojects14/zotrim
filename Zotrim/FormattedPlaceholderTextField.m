//
//  FormattedPlaceholderTextField.m
//  Zotrim
//
//  Created by Dexter Richard on 1/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "FormattedPlaceholderTextField.h"

@implementation FormattedPlaceholderTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawPlaceholderInRect:(CGRect)rect
{
    UIFont* purpleFont=[UIFont boldSystemFontOfSize:15];
    NSMutableParagraphStyle* style=[[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    style.lineHeightMultiple=1.25;
    NSDictionary* attributes=@{NSFontAttributeName: purpleFont, NSForegroundColorAttributeName: [UIColor colorWithRed:0.19 green:0.17 blue:0.4 alpha:1.0],NSParagraphStyleAttributeName: style};
    [self.placeholder drawInRect:rect withAttributes:attributes];    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
