//
//  GraphNode.m
//  Zotrim
//
//  Created by Dexter Richard on 6/7/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "GraphNode.h"
#import "GraphNode.h"
#import "ProgressGraph.h"


@implementation GraphNode

@dynamic date;
@dynamic weight;
@dynamic parentGraphOld;
@dynamic nextNode;
@dynamic previousNode;
@dynamic parentGraphNew;

@end
