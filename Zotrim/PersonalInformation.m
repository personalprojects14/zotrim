//
//  PersonalInformation.m
//  Zotrim
//
//  Created by Dexter Richard on 6/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "PersonalInformation.h"
#import "User.h"


@implementation PersonalInformation

@dynamic age;
@dynamic breakfastTime;
@dynamic dinnerTime;
@dynamic email;
@dynamic firstName;
@dynamic gender;
@dynamic goalWeight;
@dynamic height;
@dynamic initialWeight;
@dynamic lastName;
@dynamic lunchTime;
@dynamic alarmsForUser;
@dynamic username;
@dynamic user;

@end
