//
//  UserSettings.h
//  Zotrim
//
//  Created by Dexter Richard on 6/29/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface UserSettings : NSManagedObject

@property (nonatomic, retain) NSNumber * autoReminders;
@property (nonatomic, retain) NSNumber * stayLoggedIn;
@property (nonatomic, retain) NSNumber * loggedOutReminders;
@property (nonatomic, retain) User *user;

@end
