//
//  FixedSizeQueue.h
//  Zotrim
//
//  Created by Dexter Richard on 2/21/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "GenericSizedQueue.h"

@interface FixedSizeQueue : NSObject<GenericSizedQueue, NSCoding, NSFastEnumeration>

/**
 Initializes a new FixedSizeQueue with the requested maximum size. All new instances of this class should be created with this method as opposed to superclass methods.
 
 @return An instance of the initialized FixedSizeQueue with the requested capacity.
 **/
- (instancetype)initWithMaxSize:(NSNumber*)maxSize;

/**
 Checks if the queue is full or not by comparing the current number of items to maxSize.
 
 @return YES if the queue is full, NO otherwise.
 **/
- (BOOL)isFull;

/**
 Returns the object at the given position in the queue. Does not remove it from or change its position in the queue. In some implementations this message may be forwarded to a reciever that is a container, instead of being defined.
 
 @param The position in the queue that you want to retreive an item from.
 
 @return The object at the specified position in the queue
 **/
 
- (id)objectAtIndex:(NSUInteger)index;

@end
