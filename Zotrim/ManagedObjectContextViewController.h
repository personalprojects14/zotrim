//
//  ManagedObjectContextViewController.h
//  Zotrim
//
//  Created by Dexter Richard on 2/3/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalInformation.h"
#import "StateInformation.h"
#import "User.h"
#import "UINavigationController+SimpleErrorMessage.h"
#import "ConfigStrings.h"
#import "FixedSizeQueue.h"

@interface ManagedObjectContextViewController : UIViewController

@property (retain,nonatomic) NSManagedObjectContext* managedObjectContext;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

+ (PersonalInformation*)loadCurrentUserInfoFromManagedObjectContext:(NSManagedObjectContext*)context;
+ (User*)loadCurrentUserFromManagedObjectContext:(NSManagedObjectContext*)context;
+ (UserSettings*)loadCurrentUserSettingsFromManagedObjectContext:(NSManagedObjectContext*)context;
+ (StateInformation*)loadCurrentStateInfoFromManagedObjectContext:(NSManagedObjectContext*)context;

@end
