//
//  LoginScreenViewController.m
//  Zotrim
//
//  Created by Dexter Richard on 1/31/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"LoginAttempt"])
    {
        if ([self shouldLogin])
        {
            [self userDidLogin];
            return YES;
        }
        
        else
        {
            return NO;
        }
    }
    
    else
    {
        return YES;
    }
}

- (void)userDidLogin
{
    StateInformation* stateInfo=[ManagedObjectContextViewController loadCurrentStateInfoFromManagedObjectContext:[self managedObjectContext]];
    
    NSString* username=_usernameField.text;    
    stateInfo.currentUsername=username;
    
    UserSettings* settings=[ManagedObjectContextViewController loadCurrentUserSettingsFromManagedObjectContext:[self managedObjectContext]];
    if ([settings stayLoggedIn])
    {
        stateInfo.loggedIn=[NSNumber numberWithBool:YES];
    }
    
    FixedSizeQueue* recentUsers=stateInfo.recentUsers;
    [recentUsers enqueue:username];
    
    NSError* error;
    //save the changes we just made
    if (![[self managedObjectContext] save:&error])
    {
        //handle the error
        [UINavigationController displayErrorPopupWithMessage:errorSavingData];
    }
}

- (BOOL)shouldLogin
{
    NSString* username=_usernameField.text;
    NSString* enteredPassword=_passwordField.text;
    NSString* retrievedPassword=[SimpleKeychain load:username];
    
    if (retrievedPassword == NULL)
    {
        //username was not found
        [UINavigationController displayErrorPopupWithMessage:usernameNotFoundErrorMessage];
        
        return NO;
    }
    
    else if (![enteredPassword isEqualToString:retrievedPassword])
    {
        //passwords didn't match
        [UINavigationController displayErrorPopupWithMessage:incorrectPasswordErrorMessage];
        
        return NO;
    }
    
    else
    {
        return YES;
    }
}

- (void)cancelToLoginUnwindSegueAction:(UIStoryboardSegue*)segue
{
    
}

- (void)submitUnwindSegueAction:(UIStoryboardSegue*)segue
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupTextFields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateUsernameField:_username];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTextFields
{
    [_usernameField setShouldAdvance:YES];
    [_passwordField setShouldAdvance:NO];
    
    self.textFields=[[NSArray alloc] initWithObjects:_usernameField,_passwordField,nil];
}

- (void)updateUsernameField:(NSString*)newText
{
    _usernameField.text=newText;
}

- (void)autofill:(NSString *)username
{
    _username=username;
}

@end
