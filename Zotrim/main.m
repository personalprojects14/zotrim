//
//  main.m
//  Zotrim
//
//  Created by Dexter Richard on 1/22/14.
//  Copyright (c) 2014 Dexter Richard. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
